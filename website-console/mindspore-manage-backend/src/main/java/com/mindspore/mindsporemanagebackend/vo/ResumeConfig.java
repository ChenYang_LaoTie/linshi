package com.mindspore.mindsporemanagebackend.vo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@PropertySource("classpath:/properties/resume.properties")
public class ResumeConfig {

    @Value("#{'${resume.branch}'.split(',')}")
    private List<String> branches;

    @Value("#{'${resume.language}'.split(',')}")
    private List<String> languages;

    public Optional<String> getBranch(String url) {
        return this.branches.stream().filter(branch -> url.contains("/" + branch + "/")).findFirst();
    }

    public Optional<String> getLanguage(String url) {
        return this.languages.stream().filter(language -> url.contains("/" + language + "/")).findFirst();
    }
}
