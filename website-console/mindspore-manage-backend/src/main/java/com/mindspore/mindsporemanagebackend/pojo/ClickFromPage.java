package com.mindspore.mindsporemanagebackend.pojo;

/**
 * @author haha
 * @since 2020-07-23
 */
public class ClickFromPage {
    private Integer from;
    private Integer fromCounts;

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getFromCounts() {
        return fromCounts;
    }

    public void setFromCounts(Integer fromCounts) {
        this.fromCounts = fromCounts;
    }
}
