package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 文章对象.
 */
@Accessors(chain = true)
@Data
public class Essay implements Serializable {
    private static final long serialVersionUID = 253403017949857210L;

    private Integer id;

    private Integer score;

    private String title;

    private String parent;

    private String time;
}
