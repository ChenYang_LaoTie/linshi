package com.mindspore.mindsporemanagebackend.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
public class PageObject<T> implements Serializable {

    private static final long serialVersionUID = -1651480678984413869L;

    private boolean triggerSuggest;

    private String keyword;

    private String newKeyword;

    private Integer page=0;//当前页码值

    private  Integer size=10; //页面大小

    private Map totalNum; //总条数

    private List<T> records;  ///当前页记录
}