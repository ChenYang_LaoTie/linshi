package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.MsNews;
import com.mindspore.mindsporemanagebackend.pojo.NewsDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsEditorMapper {

    int getNewsCount(@Param("type") Integer type, @Param("newsTitle") String newsTitle, @Param("tag") String tag);

    List<MsNews> selectMindnews(@Param("startIndex") int startIndex, @Param("pageSize") int pageSize, @Param("type") Integer type, @Param("newsTitle") String newsTitle, @Param("tag") String tag);

    void insertNews(MsNews news);

    void insertNewDetail(NewsDetail detail);

    List<MsNews> selectNewsByIds(@Param("ids") Integer[] ids);

    void deleteWebNewsById(Integer id);

    void deleteNews(@Param("ids") Integer... ids);

    MsNews selectNewsById(Integer id);

    void updateNewsById(MsNews news);

    void updateNewsDescById(NewsDetail detail);

    NewsDetail selectNewsDescById(Integer id);

    void insertWebNews(MsNews msNews);
}
