package com.mindspore.mindsporemanagebackend.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mindspore.mindsporemanagebackend.common.constant.ResumeTypeConstant;
import com.mindspore.mindsporemanagebackend.common.util.ExcelUtil;
import com.mindspore.mindsporemanagebackend.common.util.TimeUtil;
import com.mindspore.mindsporemanagebackend.common.util.ZipFilesUtil;
import com.mindspore.mindsporemanagebackend.pojo.Resume;
import com.mindspore.mindsporemanagebackend.service.ResumeService;

import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/source")
@PropertySource(value = {"classpath:/properties/resume.properties"})
@Slf4j
public class RecruitingController {

    private static final String[] headers = {"序号", "名称", "电话", "招募类型", "邮箱", "工作单位", "职务职称", "职业经历", "技术特长", "培训经历", "报名时间"};
    private static final String[] columns = {ExcelUtil.ORDER_NUMBER, "name", "phone", "postType", "email", "company", "job", "jobExperience", "technicalExperties", "trainningExperience", "postTime"};

    @Value("${resume.saveDir}")
    private String saveDir;

    @Autowired
    private ResumeService resumeService;

    @RequestMapping(path = "/recruit/export", method = RequestMethod.GET)
    public byte[] export(Long start, Long end, HttpServletResponse response) {
        Collection<Resume> list = resumeService.queryResumes(TimeUtil.getDate(start), TimeUtil.getDate(end), null, null);
        for (Resume resume : list) {
            resume.setPostType(ResumeTypeConstant.RESUME_TYPE.get(resume.getPostType()));
        }
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=Recruiting statistics.xls");
        return ExcelUtil.export("布道师招募统计表", headers, columns, list);
    }

    @RequestMapping(path = "/recruit/download", method = RequestMethod.GET)
    public void download(Long start, Long end, HttpServletResponse response) {
        List<Resume> list = resumeService.queryResumes(TimeUtil.getDate(start), TimeUtil.getDate(end), null, null);
        List<File> files = new ArrayList<>();
        for (Resume resume : list) {
            String path = saveDir + resume.getResumePath();
            File file = new File(path);
            if (file.exists()) {
                files.add(file);
            } else {
                log.error("Not found {}. skip it.", path);
            }
        }
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=Recruiting resumes.zip");
        try {
            ZipFilesUtil.zip(files, response.getOutputStream());
        } catch (IOException e) {
            log.error("zip failed.", e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
