package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author xxx
 * @version 2
 */
@Accessors(chain = true)
@Data
public class Information implements Serializable {
    private static final long serialVersionUID = -2465909119556123768L;
    private Integer id;
    /**0表示未发送,1表示已发送*/
    private Integer status;
    private String title;
    /**对应新闻的id*/
    private Integer newsId;
    private String content;
    private String url;
    private String newsTime;
    private String createdTime;
    private String language;
}
