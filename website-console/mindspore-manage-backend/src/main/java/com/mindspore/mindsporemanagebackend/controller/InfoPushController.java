package com.mindspore.mindsporemanagebackend.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindspore.mindsporemanagebackend.common.util.ExecutionUtil;
import com.mindspore.mindsporemanagebackend.pojo.Email;
import com.mindspore.mindsporemanagebackend.pojo.Information;
import com.mindspore.mindsporemanagebackend.pojo.request.PushInfoRequest;
import com.mindspore.mindsporemanagebackend.service.InfoPushService;
import com.mindspore.mindsporemanagebackend.service.SubscripeService;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;

@RestController
@RequestMapping("/source")
public class InfoPushController {

    @Autowired
    private InfoPushService infoPushService;

    @Autowired
    private SubscripeService subscripeService;



    @RequestMapping(path = "/saveInfo", method = RequestMethod.POST)
    public SysResult saveInfo(Information info) {
        infoPushService.saveInfo(info);
        return SysResult.ok();
    }

    @RequestMapping(path = "/selectInfo", method = RequestMethod.GET)
    public SysResult selectInfo(Integer pageCurrent, Integer limit) {
        PageObject<Information> po = infoPushService.selectInfo(pageCurrent, limit);
        return SysResult.ok("查询成功", po);
    }

    @RequestMapping(path = "/selectInfoById", method = RequestMethod.GET)
    public SysResult selectInfoById(Integer id) {
        List<Information> list = infoPushService.selectInfoById(id);
        return SysResult.ok("查询成功", list);
    }

    @RequestMapping(path = "/deleteInfo", method = RequestMethod.GET)
    public SysResult deleteInfo(Integer... ids) {
        infoPushService.deleteInfo(ids);
        return SysResult.ok("删除成功");
    }

    @RequestMapping(path = "/updateInfo", method = RequestMethod.POST)
    public SysResult updateInfo(Information info) {
        infoPushService.updateInfo(info);
        return SysResult.ok();
    }

    @RequestMapping(path = "/previewInfo", method = RequestMethod.GET)
    public List<Information> previewInfo(Integer... ids) {
        return infoPushService.selectInfoById(ids);
    }

    @RequestMapping(path = "/pushInfo", method = RequestMethod.POST)
    public SysResult pushInfo(@RequestBody PushInfoRequest pushInfoRequest) {
        List<Information> list = infoPushService.selectInfoById(pushInfoRequest.getIds());
        final String uuid = UUID.randomUUID().toString();
        List<Email> emailList = subscripeService.selectEmailByLang(list.get(0).getLanguage());

        try {
            infoPushService.pushInfoEmail(pushInfoRequest.getSubject(), pushInfoRequest.getEmail(), list, null,
                    list.get(0).getLanguage());
        } catch (Exception e) {
            System.out.println("Failed to send newsletter " + e.getMessage());
        }

        ExecutionUtil.exec(() -> {
            for (Email mail : emailList) {
                try {
                    infoPushService.pushInfoEmail(pushInfoRequest.getSubject(), null, list, mail.getMail(), mail.getLang());
                } catch (Exception e) {
                    System.out.println("Failed to send newsletter " + e.getMessage());
                } finally {
                
                }

            }
            // 把发送成功的邮件状态码改变.重新存入数据库
            for (Information info : list) {
                infoPushService.updateInfoAfterSend(info);
            }
        });
        return SysResult.ok(uuid);
    }

    @GetMapping("/pushInfoTask")
    public SysResult getPushInfoTask(@RequestParam String taskId) {
        Map<String, Object> map = new HashMap<>(4);
        return SysResult.ok(map);
    }

    @RequestMapping(path = "/send2all", method = RequestMethod.POST)
    public SysResult send2All(@RequestBody PushInfoRequest pushInfoRequest) {
        List<Email> emailList = subscripeService.push();
        List<Information> information = infoPushService.selectInfoById(pushInfoRequest.getIds());
        try {
            infoPushService.pushInfoEmail(pushInfoRequest.getSubject(), pushInfoRequest.getEmail(), information, null,
                    information.get(0).getLanguage());
        } catch (Exception e) {
            System.out.println("Failed to send newsletter " + e.getMessage());
        }

        final String uuid = UUID.randomUUID().toString();
        ExecutionUtil.exec(() -> {
            for (Email mail : emailList) {
                try {
                    infoPushService.pushInfoEmail(pushInfoRequest.getSubject(), null, information, mail.getMail(),
                            mail.getLang());
                } catch (Exception e) {
                    System.out.println("Failed to send newsletter " + e.getMessage());
                } finally {
            
                }
            }

            for (Information info : information) {
                infoPushService.updateInfoAfterSend(info);
            }
        });
        return SysResult.ok(uuid);
    }
}
