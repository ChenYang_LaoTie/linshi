package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author Administrator 论文
 *
 */
@Accessors(chain = true)
@Data
public class Dissertation implements Serializable {
    private static final long serialVersionUID = -1007768849203415524L;
    private Integer id;
    private String title;

    /**
     * 摘要
     */
    private String summary;
    /**
     * 领域 1-图像 2-文本 3-音频 4-推荐 9-其他
     */
    private Integer domain;
    
    private String domainName;
    /**
     * 发表单位
     */
    private String publishedBy;
    /**
     * 发表时间
     */
    private String postedOn;
    /**
     * 来源
     */
    private Integer sources;
    
    private String sourcesName;
    /**
     * 跳转链接
     */
    private String link;
    /**
     * 状态码 0表示未发布.1表示已经发布
     */
    private Integer status = 0;
    /**
     * zh-中文 en-英文
     */
    private String lang;
    private String creatTime;

    /**
     * mindspore首发 0-否 1-是
     */
    private Integer mindspore;
    /**
     * 代码链接
     */
    private String codeLink;
}
