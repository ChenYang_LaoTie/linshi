package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author
 * @version 2
 */
@Data
@Accessors(chain = true)
public class NewsDetail {
    private Integer newsId;
    /** 新闻详情信息 */
    private String newsDetail;
    private String creatTime;
    private String modifyTime;
    /** 区分中英文的标签 */
    private String tag;
    /** 随机的参数,保证同一篇文章,中英文随机数相同 */
    private String random;
    /**
     * 0-全部 1 - 版本发布 2 - 技术博客 3 - 社区活动4-新闻
     */
    private Integer type;
    /**
     * 技术博客分类
     */
    private String category;
}
