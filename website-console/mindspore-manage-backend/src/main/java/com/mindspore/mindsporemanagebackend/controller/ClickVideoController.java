package com.mindspore.mindsporemanagebackend.controller;

import com.mindspore.mindsporemanagebackend.common.constant.PageEnum;
import com.mindspore.mindsporemanagebackend.common.util.ExcelUtil;
import com.mindspore.mindsporemanagebackend.pojo.ClickFromPage;
import com.mindspore.mindsporemanagebackend.pojo.ClickStatistic;
import com.mindspore.mindsporemanagebackend.service.ClickVideoService;
import com.mindspore.mindsporemanagebackend.vo.ClickVideoExportVO;
import com.mindspore.mindsporemanagebackend.vo.SysResult;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/source")
public class ClickVideoController {
    @Autowired
    private ClickVideoService clickVideoService;

    @RequestMapping(path = "/click/video", method = RequestMethod.GET)
    public Object getClickVideoAllData(@RequestParam Integer current, @RequestParam Integer limit) {
        return SysResult.ok("select data", clickVideoService.selectStatisticByPage(current, limit));
    }

    @RequestMapping(path = "/export/clickvideo", method = RequestMethod.GET)
    public Object exportEssay2ForumCount(HttpServletRequest request, HttpServletResponse response) {
        List<ClickStatistic> source = clickVideoService.statistic(request.getParameter("begin"),
                request.getParameter("end"));
        if (source == null || source.size() == 0) {
            return SysResult.ok("时间段内没有数据");
        }

        List<String> headers = new ArrayList<>();
        headers.add("视频名称");
        headers.add("点击次数");
        headers.add("点击人数");

        for (int i = 1; i <= 4; i++) {
            headers.add(PageEnum.getValue(i));
        }
        List<ClickVideoExportVO> exportSource = new ArrayList<>();
        for (int i = 0; i < source.size(); i++) {
            ClickVideoExportVO exportVO = new ClickVideoExportVO();
            exportVO.setName(source.get(i).getName());
            exportVO.setClicks(source.get(i).getClicks());
            exportVO.setClicksByIp(source.get(i).getClicksByIp());

            List<ClickFromPage> fromPages = source.get(i).getClickFromPages();
            for (ClickFromPage clickFromPage : fromPages) {
                if (clickFromPage.getFrom() == 1) {
                    exportVO.setFrom1Counts(clickFromPage.getFromCounts());
                }
                if (clickFromPage.getFrom() == 2) {
                    exportVO.setFrom2Counts(clickFromPage.getFromCounts());
                }
                if (clickFromPage.getFrom() == 3) {
                    exportVO.setFrom3Counts(clickFromPage.getFromCounts());
                }
                if (clickFromPage.getFrom() == 4) {
                    exportVO.setFrom4Counts(clickFromPage.getFromCounts());
                }
            }
            exportSource.add(exportVO);
        }
        String[] columns = { "name", "clicks", "clicksByIp", "from1Counts", "from2Counts", "from3Counts", "from4Counts" };
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=click.xls");
        return ExcelUtil.export2("统计视频点击数据", headers, Arrays.asList(columns), exportSource);
    }
}


