package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.entity.Competition;
import com.mindspore.mindsporemanagebackend.pojo.entity.CompetitionPopular;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import org.springframework.web.multipart.MultipartFile;

public interface CompetitionService {

    void saveCompetition(Competition competition);

    String deleteCompetition(Integer[] ids);

    String publishCompetition(Integer id);

    String batchPublishCompetition(Integer[] ids);

    FileVo upload(MultipartFile uploadFile, String type);

    void saveCompetitionPopular(CompetitionPopular competitionPopular);

    String deleteCompetitionPopular(Integer id);
}
