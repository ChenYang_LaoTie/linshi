package com.mindspore.mindsporemanagebackend.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.AuthorizationFilter;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig  {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.formLogin(AbstractHttpConfigurer::disable);
        http.csrf(AbstractHttpConfigurer::disable);

        http.sessionManagement(sessionManagement -> sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        
        http.authorizeHttpRequests(authorize -> authorize.requestMatchers(HttpMethod.GET, "/token").permitAll());
        http.authorizeHttpRequests(authorize -> authorize.requestMatchers(HttpMethod.OPTIONS).permitAll());
//        http.authorizeHttpRequests(authorize -> authorize.anyRequest().authenticated());

        http.authorizeHttpRequests(authorize -> authorize.requestMatchers("/source/**").hasAuthority("USER").anyRequest().authenticated());

        http.addFilterBefore(new TenantFilter(), AuthorizationFilter.class);

        http.oauth2Login(Customizer.withDefaults());
    
        return http.build();
    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository() {
        return new InMemoryClientRegistrationRepository(this.oneidClientRegistration());
	}

    private ClientRegistration oneidClientRegistration() {
        return ClientRegistration.withRegistrationId("oneid")
            .clientId(OIDCConfig.CLIENT_ID)
            .clientSecret(OIDCConfig.CLIENT_SECRET)
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .redirectUri(OIDCConfig.REDIRECT_URL)
            .scope("openid", "profile", "email", "phone", "offline_access")
            .authorizationUri(OIDCConfig.AUTHORIZATION_URL)
            .tokenUri(OIDCConfig.TOKEN_URL)
            .userInfoUri(OIDCConfig.USER_INFO_URL)
            .userNameAttributeName(IdTokenClaimNames.SUB)
            .jwkSetUri(OIDCConfig.JWK_SET_URL)  
            .clientName("OneID")
            .build(); 
    }

}
