package com.mindspore.mindsporemanagebackend.pojo.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class PushInfoRequest {
    private String subject;
    private String email;
    private Integer[] ids;
}
