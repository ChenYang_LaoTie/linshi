package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;
import com.mindspore.mindsporemanagebackend.mapper.InfoPushMapper;
import com.mindspore.mindsporemanagebackend.mapper.NewsEditorMapper;
import com.mindspore.mindsporemanagebackend.mapper.WebnewsMapper;
import com.mindspore.mindsporemanagebackend.pojo.Information;
import com.mindspore.mindsporemanagebackend.pojo.MsNews;
import com.mindspore.mindsporemanagebackend.pojo.NewsDetail;
import com.mindspore.mindsporemanagebackend.service.NewsEditorService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NewsEditorServiceImpl implements NewsEditorService {


    @Value("${service.host}")
    private String serviceHost;
    @Value("${service.protocol}")
    private String serviceProtocol;

    @Autowired
    private NewsEditorMapper newsEditorMapper;

    @Autowired
    private InfoPushMapper infoPushMapper;

    @Autowired
    private WebnewsMapper webnewsMapper;

    @Override
    public FileVo upload(MultipartFile uploadFile, String tag) {
        FileVo fileVo = new FileVo();

        // 1.获取文件名称,adc/jpg/JPG
        String fileName = uploadFile.getOriginalFilename();
        // 2.将文件名称统统小写,方便判断
        fileName = fileName.toLowerCase();
        // 3.利用正则表达式判断
        if (!fileName.matches("^.+\\.(png|jpg|gif|webp|jpeg)$")) {
            // 表示文件类型不匹配
            fileVo.setErrno(1);
            return fileVo;
        }
        // 4.判断是否为恶意程序
        try {
            boolean isGif = fileName.toUpperCase().endsWith(".GIF");
            int height = 0;
            int width = 0;
            if (!isGif) {
                BufferedImage image = ImageIO.read(uploadFile.getInputStream());
                // 4.1获取宽带和高度
                height = image.getHeight();
                width = image.getWidth();
                // 4.2判断属性是否为0
                if (width == 0 || height == 0) {
                    fileVo.setErrno(1);
                    return fileVo;
                }
            }
            // 5.根据时间生成文件夹
            String dateDir = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
            String localDir = String.format("%s%s", PathConstants.imageDirPath, dateDir);
            File dirFile = new File(localDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            // 6.防止文件名重复
            // 6.1生成UUID
            String uuidName = UUID.randomUUID().toString().replaceAll("-", "");
            // 6.2获取文件类型.进行拼接
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            String realName = uuidName + fileType;
            // 6.3实现文件上传
            File realFile = new File(localDir + File.separator + realName);
            uploadFile.transferTo(realFile);
            fileVo.setHeight(height);
            fileVo.setWigth(width);
            fileVo.setTag(tag);
            // 设置图片虚拟访问路径
            String realurlPath = String.format("%s%s/%s", PathConstants.imageAccessPath, dateDir, realName);
            fileVo.setUrl(realurlPath);
        } catch (IOException e) {
            log.error(e.getMessage());
            fileVo.setErrno(1);// 表示为恶意程序
            return fileVo;
        }
        return fileVo;
    }

    @Override
    public PageObject<MsNews> selectMindNews(Integer pageCurrent, Integer limit, Integer type, String newsTitle, String tag) {
        PageObject<MsNews> pageObject = new PageObject<>();

        // 查找总记录数,并进行校验
        int rowCount = newsEditorMapper.getNewsCount(type, newsTitle, tag);
        if (rowCount == 0) {
            return pageObject;
        }

        Map<String, Integer> map = new HashMap<>(4);
        map.put("totalNum", rowCount);
        int startIndex = (pageCurrent - 1) * limit;
        List<MsNews> records = newsEditorMapper.selectMindnews(startIndex, limit, type, newsTitle, tag);
        pageObject.setPage(pageCurrent).setTotalNum(map).setRecords(records).setSize(limit);
        return pageObject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertNews(MsNews news, NewsDetail detail) {
        newsEditorMapper.insertNews(news);
        // 新增商品详情
        detail.setNewsId(news.getId());
        String textContent = detail.getNewsDetail().replaceAll("01023", "&nbsp;");
        detail.setNewsDetail(textContent);
        newsEditorMapper.insertNewDetail(detail);

        Information information = new Information();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        information.setCreatedTime(df.format(new Date()));
        information.setStatus(0).setContent(news.getNewsContent()).setTitle(news.getNewsTitle())
                .setUrl(serviceProtocol + serviceHost + "/news/newschildren?id=" + news.getId()).setNewsId(news.getId())
                .setNewsTime(news.getNewsTime()).setLanguage(news.getTag());
        infoPushMapper.saveInfo(information);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteWebNews(Integer[] ids) {
        List<MsNews> list = newsEditorMapper.selectNewsByIds(ids);
        for (MsNews msNews : list) {
            if (msNews.getStatus() == 1) {
                newsEditorMapper.deleteWebNewsById(msNews.getId());
            }
        }
        for (MsNews msNews : list) {
            infoPushMapper.deleteInfoBynewsId(msNews.getId());
        }
        newsEditorMapper.deleteNews(ids);
    }

    @Override
    public MsNews selectNewsById(Integer id) {
        MsNews news = newsEditorMapper.selectNewsById(id);
        return news;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateNewsById(MsNews news, NewsDetail detail) {
        MsNews msNews = newsEditorMapper.selectNewsById(news.getId());
        news.setStatus(msNews.getStatus());
        newsEditorMapper.updateNewsById(news);

        detail.setNewsId(news.getId());
        String newsDetail = detail.getNewsDetail().replaceAll("01023", "&nbsp;");
        detail.setNewsDetail(newsDetail);
        newsEditorMapper.updateNewsDescById(detail);

        if (news.getStatus() == 1) {
            webnewsMapper.updateWebnews(news);
        }
    }

    @Override
    public NewsDetail selectNewsDescById(Integer id) {
        NewsDetail detail = newsEditorMapper.selectNewsDescById(id);
        MsNews msNews = newsEditorMapper.selectNewsById(id);
        detail.setType(msNews.getType());
        detail.setCategory(msNews.getCategory());
        return detail;
    }

    @Override
    public void updateNewsDescById(NewsDetail detail) {
        newsEditorMapper.updateNewsDescById(detail);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void releaseNews(Integer[] ids) {
        List<MsNews> list = newsEditorMapper.selectNewsByIds(ids);
        // 把要发布的新闻数据同步到webnews表中
        for (MsNews msNews : list) {
            if (msNews.getStatus() == 0) {
                newsEditorMapper.insertWebNews(msNews);
                msNews.setStatus(1);
                newsEditorMapper.updateNewsById(msNews);
            }
        }
    }

}
