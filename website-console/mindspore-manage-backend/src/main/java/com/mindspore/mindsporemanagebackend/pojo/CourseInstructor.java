package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator 课程讲师
 *
 */
@Data
public class CourseInstructor implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8484856994551809340L;
    private Integer id;
    /**
     * 讲师名称
     */
    private String name;

    /**
     * 讲师职称
     */
    private String title;
    /**
     * 讲师头像
     */
    private String avatarImg;

    private String updateTime;

    private String course;
}
