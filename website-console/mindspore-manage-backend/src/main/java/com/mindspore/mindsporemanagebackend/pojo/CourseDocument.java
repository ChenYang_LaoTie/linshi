package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 课程文档 文档路径 文档有大小 下载量 文件名 路径
 * 
 * @author Administrator
 *
 */
@Data
public class CourseDocument implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1717696128446293620L;

    private Integer id;

    private Integer courseId;

    private String name;

    private String url;

    private String size;

    private Integer download;
}
