package com.mindspore.mindsporemanagebackend.vo;

/**
 * @author haha
 * @since 2020-7-24
 * @version 1
 */
public class ClickVideoExportVO {
    private String name;
    private Integer clicks;
    private Integer clicksByIp;
    private Integer from1Counts;
    private Integer from2Counts;
    private Integer from3Counts;
    private Integer from4Counts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getClicksByIp() {
        return clicksByIp;
    }

    public void setClicksByIp(Integer clicksByIp) {
        this.clicksByIp = clicksByIp;
    }

    public Integer getFrom1Counts() {
        return from1Counts;
    }

    public void setFrom1Counts(Integer from1Counts) {
        this.from1Counts = from1Counts;
    }

    public Integer getFrom2Counts() {
        return from2Counts;
    }

    public void setFrom2Counts(Integer from2Counts) {
        this.from2Counts = from2Counts;
    }

    public Integer getFrom3Counts() {
        return from3Counts;
    }

    public void setFrom3Counts(Integer from3Counts) {
        this.from3Counts = from3Counts;
    }

    public Integer getFrom4Counts() {
        return from4Counts;
    }

    public void setFrom4Counts(Integer from4Counts) {
        this.from4Counts = from4Counts;
    }
}
