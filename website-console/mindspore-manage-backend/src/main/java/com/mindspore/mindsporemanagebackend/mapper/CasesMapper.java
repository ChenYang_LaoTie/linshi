package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.Cases;
import com.mindspore.mindsporemanagebackend.pojo.CasesCategory;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CasesMapper {

    @Select("SELECT * FROM cases_category ORDER BY superId ASC,sequenceNo ASC,id ASC")
    List<CasesCategory> getCasesCategoryList();

    void saveOrUpdateCasesCategory(CasesCategory courseCategory);

    @Select("SELECT * FROM cases_category WHERE id = #{id} ORDER BY sequenceNo ASC,id ASC")
    CasesCategory findCasesCategoryById(@Param("id") Integer id);

    @Select("SELECT * FROM cases_category WHERE superId = #{superId} ORDER BY sequenceNo ASC,id ASC")
    List<CasesCategory> getCasesCategoryListBySuperId(@Param("superId") Integer superId);

    @Select("SELECT * FROM cases WHERE categoryId = #{categoryId}")
    List<Cases> getCasesListByCategoryId(@Param("categoryId") Integer categoryId);

    void deleteCasesCategoryByIds(@Param("ids") Integer... ids);

    Integer saveCases(Cases cases);

    Integer updateCases(Cases cases);

    int getCasesCount(@Param("name") String name, @Param("categoryId") Integer categoryId);

    List<Cases> getCasesList(@Param("startIndex") int startIndex, @Param("pageSize") Integer pageSize, @Param("name") String name, @Param("categoryId") Integer categoryId);

    void releaseCases(@Param("ids") Integer[] ids);

    void unshelfCases(@Param("ids") Integer[] ids);

    void deleteCases(@Param("ids") Integer[] ids);
}
