package com.mindspore.mindsporemanagebackend.pojo.enums;

/**
 * 发布状态
 *
 * @author yuanyan
 * @version 0.1
 * @create 2021/12/13
 */
public enum PublishStatus {
    UN_PUBLISHED("UN_PUBLISHED", "未发布"),
    PUBLISHED("PUBLISHED", "已发布");

    private String status;

    private String description;

    PublishStatus(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
