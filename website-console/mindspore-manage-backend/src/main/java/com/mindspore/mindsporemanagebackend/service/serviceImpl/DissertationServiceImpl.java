package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;
import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.common.util.TimeUtil;
import com.mindspore.mindsporemanagebackend.mapper.DissertationMapper;
import com.mindspore.mindsporemanagebackend.pojo.Dissertation;
import com.mindspore.mindsporemanagebackend.pojo.DissertationCategory;
import com.mindspore.mindsporemanagebackend.pojo.DissertationSearch;
import com.mindspore.mindsporemanagebackend.service.DissertationService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DissertationServiceImpl implements DissertationService {

    @Autowired
    private DissertationMapper dissertationMapper;

    static final String TYPE_ERROR = "类型不匹配";
    static final Integer SEQUENCENO = 999;

    @Override
    public PageObject<Dissertation> getDissertationList(DissertationSearch search) {
        PageObject<Dissertation> pageObject = new PageObject<>();
        if (search.getPage() == null || search.getPage() < 1) {
            throw new ServiceException("当前页码值不正确");
        }
        Map<String, Integer> map = new HashMap<>(4);
        int startIndex = (search.getPage() - 1) * search.getPageSize();
        search.setStartIndex(startIndex);
        int rowCount = dissertationMapper.getRowCount(search);
        if (rowCount == 0) {
            return pageObject;
        }
        map.put("totalNum", rowCount);
        List<Dissertation> records = dissertationMapper.getDissertationList(search);
        HashMap<Integer, String> categoryMap = getCategoryMap();
        for (int i = 0; i < records.size(); i++) {
            records.get(i).setDomainName(categoryMap.get(records.get(i).getDomain()));
            records.get(i).setSourcesName(categoryMap.get(records.get(i).getSources()));
        }

        pageObject.setPage(search.getPage()).setTotalNum(map).setRecords(records).setSize(search.getPageSize());
        return pageObject;
    }

    @Override
    public HashMap<Integer, String> getCategoryMap() {
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        List<DissertationCategory> list = dissertationMapper.getDissertationCategory();
        if (list != null && list.size() > 0) {
            for (DissertationCategory category : list) {
                map.put(category.getId(), category.getTitle());
            }
        }
        return map;
    }

    @Override
    public void insertDissertation(Dissertation dissertation) {
        dissertation.setStatus(0);
        dissertation.setCreatTime(TimeUtil.getDate(System.currentTimeMillis()));
        dissertationMapper.insertDissertation(dissertation);
    }

    @Override
    public void updateDissertation(Dissertation dissertation) {
        dissertationMapper.updateDissertation(dissertation);
    }

    @Override
    public void releaseOrUnReleaseDissertation(boolean isRelease, Integer id) {
        dissertationMapper.updateDissertationStatus(isRelease == true ? 1 : 0, id);
    }

    @Override
    public void deleteDissertation(Integer... ids) {
        dissertationMapper.deleteDissertation(ids);
    }

    @Override
    public FileVo uploadImageFile(MultipartFile uploadFile, String type) {
        FileVo fileVo = new FileVo();

        // 1.获取文件名称,adc/jpg/JPG
        String fileName = uploadFile.getOriginalFilename();
        fileVo.setTrueName(fileName);
        if (type.equals(StringConstants.POPULAR_RECOMMENDATION_COVER)) {
            // 3.利用正则表达式判断
            if (!fileName.matches("^.+\\.(png|jpg|PNG|JPG|jpeg|JPEG)$")) {
                // 表示文件类型不匹配
                fileVo.setTag(TYPE_ERROR);
                fileVo.setErrno(1);
                return fileVo;
            }
        }
        // 4.判断是否为恶意程序
        try {
            boolean isGif = fileName.toUpperCase().endsWith(".GIF");
            int height = 0;
            int width = 0;
            if (!isGif) {
                if (type.equals(StringConstants.POPULAR_RECOMMENDATION_COVER)) {
                    BufferedImage image = ImageIO.read(uploadFile.getInputStream());
                    // 4.1获取宽带和高度
                    height = image.getHeight();
                    width = image.getWidth();
                    // 4.2判断属性是否为0
                    if (width == 0 || height == 0) {
                        fileVo.setErrno(1);
                        return fileVo;
                    }
                }
            }
            // 5.根据时间生成文件夹
            String localDir = String.format("%s%s", PathConstants.imageDirPath, type);
            File dirFile = new File(localDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            // 6.防止文件名重复
            // 6.1生成UUID
            String uuidName = UUID.randomUUID().toString().replaceAll("-", "");
            // 6.2获取文件类型.进行拼接
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            String realName = uuidName + fileType;
            // 6.3实现文件上传
            File realFile = new File(localDir + File.separator + realName);
            uploadFile.transferTo(realFile);
            fileVo.setHeight(height);
            fileVo.setWigth(width);
            // 设置图片虚拟访问路径
            String realurlPath = String.format("%s%s/%s", PathConstants.imageAccessPath, type, realName);
            fileVo.setUrl(realurlPath);
            fileVo.setSize(FileUtils.byteCountToDisplaySize(realFile.length()));
        } catch (IOException e) {
            log.error(e.getMessage());
            fileVo.setErrno(1);// 表示为恶意程序
            return fileVo;
        }
        return fileVo;
    }

    @Override
    public void insertDissertationCategory(DissertationCategory category) {
        category.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        if (!DissertationCategory.POPULAR.equals(category.getType())) {
            if (category.getSequence() == null || category.getSequence() == 0) {
                category.setSequence(SEQUENCENO);
            }
        }
        dissertationMapper.insertDissertationCategory(category);
    }

    @Override
    public void updateDissertationCategory(DissertationCategory category) {
        category.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        if (!DissertationCategory.POPULAR.equals(category.getType())) {
            if (category.getSequence() == null || category.getSequence() == 0) {
                category.setSequence(SEQUENCENO);
            }
        }
        dissertationMapper.updateDissertationCategory(category);
    }

    @Override
    public String deleteDissertationCategory(Integer id) {
        DissertationCategory dc = dissertationMapper.getDissertationCategoryById(id);
        if (dc != null) {
            if (dc.getType().equals(DissertationCategory.DOMAIN) || dc.getType().equals(DissertationCategory.SOURCE)) {
                List<Dissertation> list = dissertationMapper.getDissertationListByDomainOrSources(dc.getId());
                if (list != null && list.size() > 0) {
                    return "类型已引用，禁止删除！";
                } else {
                    dissertationMapper.deleteDissertationCategory(id);
                }
            } else {
                dissertationMapper.deleteDissertationCategory(id);
            }
        }
        return null;
    }

    @Override
    public HashMap<String, List<DissertationCategory>> getDissertationCategoryList(String lang) {
        HashMap<String, List<DissertationCategory>> map = new HashMap<String, List<DissertationCategory>>();
        List<DissertationCategory> popular = dissertationMapper.getDissertationCategoryList(lang, DissertationCategory.POPULAR);
        map.put("popular", popular);
        List<DissertationCategory> domain = dissertationMapper.getDissertationCategoryList(lang, DissertationCategory.DOMAIN);
        map.put("domain", domain);
        List<DissertationCategory> source = dissertationMapper.getDissertationCategoryList(lang, DissertationCategory.SOURCE);
        map.put("source", source);
        return map;
    }

}
