package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Example;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import org.springframework.web.multipart.MultipartFile;

public interface ExampleService {

    FileVo upload(MultipartFile uploadFile, String exampleIcon);

    void addOrUpdateExample(Example example);

    PageObject<Example> getExampleList(Integer pageCurrent, Integer limit);

    String releaseExample(Integer[] ids);

    String unshelfExample(Integer[] ids);

    String deleteExample(Integer[] ids);
}
