package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Information;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

import java.util.List;

public interface InfoPushService {

    void saveInfo(Information info);

    PageObject<Information> selectInfo(Integer pageCurrent, Integer limit);

    List<Information> selectInfoById(Integer... ids);

    void deleteInfo(Integer... ids);

    void updateInfo(Information info);

    void pushInfoEmail(String subject, String email, List<Information> list, String address, String language) throws Exception;

    void updateInfoAfterSend(Information info);

}
