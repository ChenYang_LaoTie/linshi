package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 热门课程推荐
 *
 * @author yuanyan
 * @version 0.1
 * @create 2021/10/27
 */
@Data
public class CoursePopular implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -560965164943398564L;

    private Integer id;

    // 热门课程推荐名称
    private String name;

    // 热门课程推荐连接
    private String link;

    // 热门课程推荐图片
    private String image;

    // 语言
    private String lang;

    // 排序
    private Integer sequence;

    private String createTime;

    private String updateTime;
}
