package com.mindspore.mindsporemanagebackend.controller;

import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.pojo.entity.Competition;
import com.mindspore.mindsporemanagebackend.pojo.entity.CompetitionPopular;
import com.mindspore.mindsporemanagebackend.service.CompetitionService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.SysResult;
import io.micrometer.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/source")
public class CompetitionController {

    @Autowired
    private CompetitionService competitionService;

    @RequestMapping(path = "/competition/save", method = RequestMethod.POST)
    public SysResult saveCompetition(Competition competition) {
        if (StringUtils.isEmpty(competition.getLang())) {
            competition.setLang("zh");
        }
        if (competition.getTitle().contains("&amp;")) {
            competition.setTitle(competition.getTitle().replaceAll("amp;", ""));
        }
        if (competition.getIntroduce().contains("&amp;")) {
            competition.setIntroduce(competition.getIntroduce().replaceAll("amp;", ""));
        }
        if (competition.getOrganizer().contains("&amp;")) {
            competition.setOrganizer(competition.getOrganizer().replaceAll("amp;", ""));
        }

        competitionService.saveCompetition(competition);
        return SysResult.ok();
    }

    @RequestMapping(path = "/competition/delete", method = RequestMethod.GET)
    public SysResult deleteCompetition(Integer[] ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要删除项", null);
        }
        String result = competitionService.deleteCompetition(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("竞赛活动删除失败", result);
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/competition/publish", method = RequestMethod.GET)
    public SysResult publishCompetition(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要发布项", null);
        }
        String result = competitionService.publishCompetition(id);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("竞赛活动发布失败", result);
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/competition/batchPublish", method = RequestMethod.GET)
    public SysResult batchPublishCompetition(Integer[] ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要发布项", null);
        }
        String result = competitionService.batchPublishCompetition(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("竞赛活动发布失败", result);
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/competitionpopular/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCompetitionPopularFile(MultipartFile uploadFile) {
        return competitionService.upload(uploadFile, StringConstants.COMPETITION_POPULAR);
    }

    @RequestMapping(path = "/competitionpopular/save", method = RequestMethod.POST)
    public SysResult saveCompetitionPopular(CompetitionPopular competitionPopular) {
        if (StringUtils.isEmpty(competitionPopular.getLang())) {
            competitionPopular.setLang("zh");
        }
        if (competitionPopular.getName().contains("&amp;")) {
            competitionPopular.setName(competitionPopular.getName().replaceAll("amp;", ""));
        }

        competitionService.saveCompetitionPopular(competitionPopular);
        return SysResult.ok();
    }

    @RequestMapping(path = "/competitionpopular/delete", method = RequestMethod.GET)
    public SysResult deleteCompetitionPopular(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要删除项", null);
        }
        String result = competitionService.deleteCompetitionPopular(id);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("热门竞赛推荐删除失败", result);
        }
        return SysResult.ok();
    }
}
