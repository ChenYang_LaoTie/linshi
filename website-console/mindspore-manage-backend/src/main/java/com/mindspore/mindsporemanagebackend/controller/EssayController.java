package com.mindspore.mindsporemanagebackend.controller;

import com.mindspore.mindsporemanagebackend.common.util.CommUtil;
import com.mindspore.mindsporemanagebackend.common.util.ExcelUtil;
import com.mindspore.mindsporemanagebackend.common.util.TimeUtil;
import com.mindspore.mindsporemanagebackend.pojo.Essay2Forum;
import com.mindspore.mindsporemanagebackend.pojo.ExportEssay;
import com.mindspore.mindsporemanagebackend.pojo.entity.EssayJump;
import com.mindspore.mindsporemanagebackend.pojo.response.EssayScoreSummary;
import com.mindspore.mindsporemanagebackend.service.EssayService;
import com.mindspore.mindsporemanagebackend.vo.PageResult;
import com.mindspore.mindsporemanagebackend.vo.SysResult;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/source")
public class EssayController {
    private static final String[] headers = {"序号", "版块", "二级版块", "语言", "分支", "名称", "1星", "2星", "3星", "4星", "5星"};
    private static final String[] columns = {ExcelUtil.ORDER_NUMBER, "parent", "twoParent", "language", "branch", "title", "oneGrade", "twoGrade", "threeGrade", "fourGrade", "fiveGrade"};

    @Autowired
    private EssayService essayService;

    @RequestMapping(path = "/export", method = RequestMethod.GET)
    public Object export(Long start, Long end, HttpServletResponse response) throws Exception {
        // 由于数据原因。截断2020-05-16 00:00:00之前的数据
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime parse = LocalDateTime.parse("2020-05-16 00:00:00", dateTimeFormatter);
        long tempTime = LocalDateTime.from(parse).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        if (tempTime > start) {
            return SysResult.ok("2020-05-16之前的数据不支持导出");
        }
        Collection<ExportEssay> list = essayService.exportEssay(TimeUtil.getDate(start), TimeUtil.getDate(end));
        list = CommUtil.splitTwoParent(list);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=Evaluation statistics.xls");
        return ExcelUtil.export("用户评价统计表", headers, columns, list);
    }

    @RequestMapping(path = "/essay/forum/page", method = RequestMethod.GET)
    public SysResult selectAll(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return SysResult.ok("by page", essayService.selectByPage(page, limit));
    }

    @RequestMapping(path = "/export/essay/forum", method = RequestMethod.GET)
    public byte[] exportEssay2ForumCount(HttpServletRequest request, HttpServletResponse response) {
        String[] headers = {"序号", "版块", "二级版块", "语言", "分支", "名称", "次数"};
        String[] columns = {"type", "twoType", "language", "branch", "name", "count"};
        List<Essay2Forum> source = essayService.selectAll();
        source = CommUtil.splitTwoType(source);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=essay-forum.xls");
        return ExcelUtil.export("文章跳转到论坛数统计表", headers, columns, source);
    }

    @RequestMapping(path = "/queryEssayScoreSummaries", method = RequestMethod.GET)
    public PageResult<EssayScoreSummary> queryEssayScoreSummaries(Integer pageCurrent, Integer limit, String startTime, String endTime) {
        return essayService.queryEssayScoreSummaries(pageCurrent, limit, startTime, endTime);
    }

    @RequestMapping(path = "/exportEssayScoreSummaries", method = RequestMethod.GET)
    public Object exportEssayScoreSummaries(Integer pageCurrent, Integer limit, String startTime, String endTime, HttpServletResponse response) {
        Collection<EssayScoreSummary> essayScoreSummaries = essayService.getEssayScoreSummaries(pageCurrent, limit, startTime, endTime);
        String[] headers = {"序号", "文章链接", "1星", "2星", "3星", "4星", "5星", "最新评价时间"};
        String[] columns = {ExcelUtil.ORDER_NUMBER, "essayUrl", "oneCount", "twoCount", "threeCount", "fourCount", "fiveCount", "maxTime"};
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=Evaluation statistics.xls");
        return ExcelUtil.export("用户评价统计表", headers, columns, essayScoreSummaries);
    }

    @RequestMapping(path = "/queryEssayJumps", method = RequestMethod.GET)
    public PageResult<EssayJump> queryEssayJumps(Integer pageCurrent, Integer limit) {
        return essayService.queryEssayJumps(pageCurrent, limit);
    }

    @RequestMapping(path = "/exportEssayJumps", method = RequestMethod.GET)
    public byte[] exportEssayJumps(Integer pageCurrent, Integer limit, HttpServletResponse response) {
        String[] headers = {"序号", "文章链接", "次数"};
        String[] columns = {ExcelUtil.ORDER_NUMBER, "essayUrl", "jumpCount"};
        Collection<EssayJump> essayJumps = essayService.getEssayJumps(pageCurrent, limit);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=essay-forum.xls");
        return ExcelUtil.export("文章跳转到论坛数统计表", headers, columns, essayJumps);
    }
}
