package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.Resume;

import java.util.List;

public interface ResumeMapper {

    List<Resume> queryResumes(String start, String end, String name, String phone);

}
