package com.mindspore.mindsporemanagebackend.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.mindspore.mindsporemanagebackend.pojo.Course;
import com.mindspore.mindsporemanagebackend.pojo.CourseCategory;
import com.mindspore.mindsporemanagebackend.pojo.CourseDocument;
import com.mindspore.mindsporemanagebackend.pojo.CourseInstructor;
import com.mindspore.mindsporemanagebackend.pojo.CoursePopular;

public interface CourseMapper {

    
    @Select("SELECT * FROM course_popular WHERE lang = #{lang} order by sequence,id")
    List<CoursePopular> getCoursePopularList(String lang);

    @Select("SELECT * FROM course_category where lang = #{lang} ORDER BY superId ASC,sequenceNo ASC,id ASC")
    List<CourseCategory> getCourseCategoryList(String lang);

    void saveOrUpdateCoursePopular(CoursePopular coursepopular);

    @Delete("delete FROM course_popular WHERE id = #{id}")
    void deleteCoursePopular(Integer id);

    void saveOrUpdateCourseCategory(CourseCategory courseCategory);

    @Select("SELECT * FROM course_category WHERE id = #{id} ORDER BY sequenceNo ASC,id ASC")
    CourseCategory findCourseCategoryById(@Param("id") Integer id);

    @Select("SELECT * FROM course_category WHERE superId = #{superId} ORDER BY sequenceNo ASC,id ASC")
    List<CourseCategory> getCourseCategoryListBySuperId(@Param("superId") Integer superId);

    @Select("SELECT * FROM course WHERE categoryId = #{categoryId}")
    List<Course> getCourseListByCategoryId(@Param("categoryId") Integer categoryId);

    void deleteCourseCategoryByIds(@Param("ids") Integer... ids);

    void saveOrUpdateCourseInstructor(CourseInstructor instructor);

    @Select("SELECT * FROM course_category WHERE teamId LIKE #{teamId}")
    List<CourseCategory> getCourseCategoryListLikeTeamId(@Param("teamId") String teamId);

    @Delete("delete FROM course_instructor WHERE id = #{id}")
    void deleteCourseInstructorById(@Param("id") Integer id);

    @Select("SELECT * FROM course_instructor")
    List<CourseInstructor> getCourseInstructor();

    @Select("SELECT * FROM course_instructor WHERE name like concat('%',#{name},'%')")
    List<CourseInstructor> searchCourseInstructor(String name);

    int getCourseInstructorCount(@Param("name") String name);

    List<CourseInstructor> getCourseInstructorList(@Param("startIndex") int startIndex, @Param("pageSize") Integer pageSize, @Param("name") String name);

    @Select("SELECT * FROM course_category where level = #{level} ORDER BY superId ASC,sequenceNo ASC,id ASC")
    List<CourseCategory> getCourseCategoryListByLevel(Integer level);

    Integer saveCourse(Course course);

    @Insert("insert into course_document (courseId,name,url,size,download) values (#{courseId},#{doc.name},#{doc.url},#{doc.size},0)")
    void saveCourseDocument(int courseId, CourseDocument doc);

    Integer updateCourse(Course course);

    @Delete("delete FROM course_document WHERE courseId = #{courseId}")
    void deleteCourseDocumentByCourseId(@Param("courseId") Integer courseId);

    @Select("SELECT * FROM course  WHERE id = #{id}")
    Course findCourseById(@Param("id") Integer id);

    @Select("select * from course_document where courseId = #{courseId}")
    List<CourseDocument> getCourseDocumentByICourseId(@Param("courseId") int courseId);

    int getCourseCount(@Param("name") String name, @Param("categoryId") Integer categoryId, @Param("lang") String lang);

    List<Course> getCourseList(@Param("startIndex") int startIndex, @Param("pageSize") Integer pageSize, @Param("name") String name, @Param("categoryId") Integer categoryId, @Param("lang") String lang);

    void releaseCourse(@Param("ids") Integer[] ids);

    void unshelfCourse(@Param("ids") Integer[] ids);

    List<Course> getCourseByIds(@Param("ids") Integer[] ids);

    void deleteCourse(@Param("ids") Integer[] ids);

}
