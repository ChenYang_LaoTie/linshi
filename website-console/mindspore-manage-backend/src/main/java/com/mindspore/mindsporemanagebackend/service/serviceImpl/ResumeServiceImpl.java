package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import com.mindspore.mindsporemanagebackend.mapper.ResumeMapper;
import com.mindspore.mindsporemanagebackend.pojo.Resume;
import com.mindspore.mindsporemanagebackend.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeMapper resumeMapper;

    @Override
    public List<Resume> queryResumes(String start, String end, String name, String phone) {
        return this.resumeMapper.queryResumes(start, end, name, phone);
    }

}
