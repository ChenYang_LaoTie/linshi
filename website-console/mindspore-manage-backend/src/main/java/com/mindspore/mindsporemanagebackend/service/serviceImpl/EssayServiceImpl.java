package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.mindspore.mindsporemanagebackend.common.util.CommUtil;
import com.mindspore.mindsporemanagebackend.mapper.EssayMapper;
import com.mindspore.mindsporemanagebackend.pojo.Essay;
import com.mindspore.mindsporemanagebackend.pojo.Essay2Forum;
import com.mindspore.mindsporemanagebackend.pojo.ExportEssay;
import com.mindspore.mindsporemanagebackend.pojo.entity.EssayJump;
import com.mindspore.mindsporemanagebackend.pojo.response.EssayScoreSummary;
import com.mindspore.mindsporemanagebackend.service.EssayService;
import com.mindspore.mindsporemanagebackend.vo.PageResult;
import com.mindspore.mindsporemanagebackend.vo.ResumeConfig;

import lombok.extern.slf4j.Slf4j;

@Service
@PropertySource(value = {"classpath:/properties/service.properties"})
@Slf4j
public class EssayServiceImpl implements EssayService {

    @Autowired
    private EssayMapper essayMapper;

    @Autowired
    private ResumeConfig resumeConfig;

    @Value("${service.host}")
    private String serviceHost;
    @Value("${service.protocol}")
    private String serviceProtocol;

    @Override
    public Collection<ExportEssay> exportEssay(String start, String end) {
        List<Essay> essays = essayMapper.queryEssays(start, end);
        Map<String, ExportEssay> map = new HashMap<>();
        for (Essay essay : essays) {
            essay.setTitle(serviceProtocol + serviceHost + essay.getTitle());
            ExportEssay exportEssay = Optional.ofNullable(map.get(essay.getTitle())).map(say -> say.addEssay(essay)).orElseGet(() -> new ExportEssay(essay));

            resumeConfig.getBranch(essay.getTitle()).ifPresent(exportEssay::setBranch);
            resumeConfig.getLanguage(essay.getTitle()).ifPresent(exportEssay::setLanguage);
            map.put(essay.getTitle(), exportEssay);
        }
        return map.values();
    }

    @Override
    public PageResult<Essay2Forum> selectByPage(int page, int limit) {
        PageResult<Essay2Forum> result = new PageResult<>();
        int total = essayMapper.getTotal();
        List<Essay2Forum> records = essayMapper.selectByPage((page - 1) * limit, limit);
        records = CommUtil.splitTwoType(records);
        result.setCurrent(page);
        result.setLimit(limit);
        result.setTotal(total);
        result.setData(records);
        return result;
    }

    @Override
    public List<Essay2Forum> selectAll() {
        return essayMapper.selectAll();
    }

    @Override
    public PageResult<EssayScoreSummary> queryEssayScoreSummaries(Integer pageCurrent, Integer limit, String startTime, String endTime) {
        // 总数量
        int essayScoreSummaryCount = essayMapper.getEssayScoreSummaryCount(startTime, endTime);
        if (essayScoreSummaryCount == 0) {
            return new PageResult<>();
        }

        PageResult<EssayScoreSummary> result = new PageResult<>();
        result.setCurrent(pageCurrent);
        result.setLimit(limit);
        result.setTotal(essayScoreSummaryCount);
        result.setData(getEssayScoreSummaries(pageCurrent, limit, startTime, endTime));
        return result;
    }

    @Override
    public List<EssayScoreSummary> getEssayScoreSummaries(Integer pageCurrent, Integer limit, String startTime, String endTime) {
        // 查询的数据
        List<EssayScoreSummary> essayScoreSummaries = (pageCurrent == null || pageCurrent < 1 || limit == null)
                ? essayMapper.queryEssayScoreSummaries(null, null, startTime, endTime)
                : essayMapper.queryEssayScoreSummaries((pageCurrent - 1) * limit, limit, startTime, endTime);
        return essayScoreSummaries;
    }

    @Override
    public PageResult<EssayJump> queryEssayJumps(Integer pageCurrent, Integer limit) {
        int essayJumpCount = essayMapper.getEssayJumpCount();
        if (essayJumpCount == 0) {
            return new PageResult<>();
        }

        PageResult<EssayJump> result = new PageResult<>();
        result.setCurrent(pageCurrent);
        result.setLimit(limit);
        result.setTotal(essayJumpCount);
        result.setData(getEssayJumps(pageCurrent, limit));
        return result;
    }

    @Override
    public List<EssayJump> getEssayJumps(Integer pageCurrent, Integer limit) {
        List<EssayJump> essayJumps = (pageCurrent == null || pageCurrent < 1 || limit == null)
                ? essayMapper.queryEssayJumps(null, null)
                : essayMapper.queryEssayJumps((pageCurrent - 1) * limit, limit);
        return essayJumps;
    }

}
