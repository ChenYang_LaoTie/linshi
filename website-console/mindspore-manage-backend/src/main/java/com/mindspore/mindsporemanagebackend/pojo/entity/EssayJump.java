package com.mindspore.mindsporemanagebackend.pojo.entity;

import lombok.Data;

/**
 * 文章跳转信息
 *
 * @author yuanyan
 * @version 0.1
 * @create 2021/12/27
 */
@Data
public class EssayJump {
    private Integer id;

    // 文章链接
    private String essayUrl;

    // 文章跳转次数
    private Integer jumpCount;

    // 跳转时间
    private String time;
}
