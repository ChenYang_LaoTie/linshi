package com.mindspore.mindsporemanagebackend.common.constant;

/**
 * @author
 * @version 2
 */
public class StringConstants {
    /**
     * API搜索类型
     */
    public static final String API = "API";
    public static final String ZH = "zh";

    public static final String SUCCESS_SUBSCRIPTION_ZH = "订阅成功";
    public static final String SUCCESS_SUBSCRIPTION_EN = "Subscription succeeded";

    public static final String SUCCESS_SUBSCRIPTION_AND_THANKS_ZH = "您已成功订阅MindSpore最新资讯，感谢您的关注！";
    public static final String SUCCESS_SUBSCRIPTION_AND_THANKS_EN = "You have successfully subscribed to the latest Mindspore news. Thank you for your attention.";

    public final static String REDIS_TOTAL_PREFIX = "total_";

    /**
     * 最新视频
     */
    public static final String VIDEO = "video";

    /**
     * 课程三级目录封面
     */
    public static final String COURSE_CATEGORY_COVER = "cover";
    /**
     * 讲师头像
     */
    public static final String COURSE_INSTRUCTOR_AVATAR = "avatar";
    /**
     * 课程热门推荐
     */
    public static final String COURSE_POPULAR = "coursepopular";
    /**
     * 热门竞赛推荐
     */
    public static final String COMPETITION_POPULAR = "competitionpopular";
    /**
     * 论文热门推荐
     */
    public static final String POPULAR_RECOMMENDATION_COVER = "popular";
    /**
     * 模型与工具热门推荐
     */
    public static final String POPULAR_MODELSANDTOOLS_COVER = "modelspopular";
    /**
     * 课程上传文件上层文件夹
     */
    public static final String COURSE = "course";
    /**
     * 课程视频
     */
    public static final String COURSE_VIDEO = "video";
    /**
     * 课程文件
     */
    public static final String COURSE_DOCUMENT = "document";
    /**
     * 课程文件
     */
    public static final String COURSE_IMAGE = "image";

    public static final String CASES_LOGO = "cases";

    public static final String EXAMPLE = "example";

    public static final String MODELED = "modeled";

    public static final String EXAMPLE_ICON = "icon";

    public static final String MODELED_ICON = "icon";

}
