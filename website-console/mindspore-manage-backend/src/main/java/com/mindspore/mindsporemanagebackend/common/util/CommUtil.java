package com.mindspore.mindsporemanagebackend.common.util;

import com.mindspore.mindsporemanagebackend.pojo.Essay2Forum;
import com.mindspore.mindsporemanagebackend.pojo.ExportEssay;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CommUtil {

    /**
     * 论坛数据分割二级板块
     * 
     * @param records
     * @return
     */
    public static List<Essay2Forum> splitTwoType(List<Essay2Forum> records) {
        List<Essay2Forum> result = new ArrayList<>();
        if (records != null && records.size() > 0) {
            int index = -1;
            for (Essay2Forum forum : records) {
                index = forum.getType().indexOf("/");
                if (index > -1) {
                    forum.setTwoType(forum.getType().substring(index + 1));
                    forum.setType(forum.getType().substring(0, index));
                } else {
                    forum.setTwoType("");
                }
                result.add(forum);
            }
        } else {
            result = records;
        }
        return result;
    }

    public static Collection<ExportEssay> splitTwoParent(Collection<ExportEssay> records) {
        Collection<ExportEssay> result = new ArrayList<>();
        if (records != null && records.size() > 0) {
            int index = -1;
            for (ExportEssay forum : records) {
                index = forum.getParent().indexOf("/");
                if (index > -1) {
                    forum.setTwoParent(forum.getParent().substring(index + 1));
                    forum.setParent(forum.getParent().substring(0, index));
                } else {
                    forum.setTwoParent("");
                }
                result.add(forum);
            }
        } else {
            result = records;
        }
        return result;
    }

    public static String searchSimple(String keyword) {
        if (keyword.indexOf("_") == -1) {
            return "";
        } else {
            if (keyword.indexOf("_") > 0 && keyword.indexOf(" ") == -1) {
                return ",\"analyzer\":\"simple\"";
            }
        }
        return "";
    }
}
