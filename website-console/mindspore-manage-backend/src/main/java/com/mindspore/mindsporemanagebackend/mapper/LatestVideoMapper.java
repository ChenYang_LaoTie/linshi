package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.LatestVideo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LatestVideoMapper {

    @Select("select count(*) from latest_video")
    Integer getRowCount();

    @Select("select * from latest_video order by id limit #{startIndex},#{pageSize}")
    List<LatestVideo> selectLatestVideo(Integer startIndex, Integer pageSize);

    @Select("select * from latest_video where id = #{id}")
    LatestVideo getLatestVideoById(@Param("id") Integer id);

    void insertLatestVideo(@Param("video") LatestVideo video);

    void updateLatestVideoById(@Param("video") LatestVideo video);

    void deleteLatestVideo(@Param("ids") Integer[] ids);

    
    @Select("select * from latest_video where lang = #{lang} and supercode is null order by sort,id desc")
    List<LatestVideo> getLatestvideoOneLevelList(@Param("lang") String lang);

    @Select("select * from latest_video where lang = #{lang} and supercode is not null order by sort,id desc")
    List<LatestVideo> getLatestvideoList(@Param("lang") String lang);
}
