package com.mindspore.mindsporemanagebackend.common.constant;

/**
 * @author haha
 * @since 2020-07-21
 * @version 1
 */
public enum PageEnum {
    INDEX(1, "来源于首页次数"), TUTORIAL(2, "来源于教程页次数"), COURSE_LOCAL(3, "来源于上传课程"), COURSE_OBS(4, "来源于OBS课程");

    private int index;
    private String name;

    PageEnum(int index, String name) {
        this.name = name;
        this.index = index;
    }

    public static String getValue(int index) {
        for (PageEnum p : PageEnum.values()) {
            if (p.getIndex() == index) {
                return p.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
