package com.mindspore.mindsporemanagebackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.pojo.Example;
import com.mindspore.mindsporemanagebackend.service.ExampleService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;

import io.micrometer.common.util.StringUtils;

@RestController
@RequestMapping("/source")
public class ExampleController {

    @Autowired
    private ExampleService exampleService;

    @RequestMapping(path = "/example/icon/uploadFile", method = RequestMethod.POST)
    public FileVo uploadImageFile(MultipartFile uploadFile) {
        return exampleService.upload(uploadFile, StringConstants.EXAMPLE_ICON);
    }

    @RequestMapping(path = "/example/add", method = RequestMethod.POST)
    public SysResult addOrUpdateExample(Example example) {
        exampleService.addOrUpdateExample(example);
        return SysResult.ok();
    }

    @RequestMapping(path = "/example/list", method = RequestMethod.GET)
    public PageObject<Example> getExampleList(Integer pageCurrent, Integer limit) {
        return exampleService.getExampleList(pageCurrent, limit);
    }

    @RequestMapping(path = "/example/release", method = RequestMethod.GET)
    public SysResult releaseExample(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要上架的示例", null);
        }
        String result = exampleService.releaseExample(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("示例上架失败", "");
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/example/unshelf", method = RequestMethod.GET)
    public SysResult unshelfExample(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要下架的示例", null);
        }
        String result = exampleService.unshelfExample(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("示例下架失败", "");
        }
        return SysResult.ok();
    }
    
    @RequestMapping(path = "/example/delete", method = RequestMethod.GET)
    public SysResult deleteExample(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要删除的示例", null);
        }
        String result = exampleService.deleteExample(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("示例删除失败", "");
        }
        return SysResult.ok();
    }
}
