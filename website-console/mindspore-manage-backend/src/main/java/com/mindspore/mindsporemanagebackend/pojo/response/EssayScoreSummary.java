package com.mindspore.mindsporemanagebackend.pojo.response;

import lombok.Data;

/**
 * 文章得分汇总
 *
 * @author yuanyan
 * @version 0.1
 * @create 2021/12/21
 */
@Data
public class EssayScoreSummary {
    // 文章链接
    private String essayUrl;
    // 文章评分一星~五星数量
    private Integer oneCount;
    private Integer twoCount;
    private Integer threeCount;
    private Integer fourCount;
    private Integer fiveCount;
    // 最新评价时间
    private String maxTime;
}