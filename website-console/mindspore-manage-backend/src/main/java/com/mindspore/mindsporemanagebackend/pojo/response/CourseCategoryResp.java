package com.mindspore.mindsporemanagebackend.pojo.response;

import com.mindspore.mindsporemanagebackend.pojo.Course;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CourseCategoryResp {

    private Integer id;

    private String name;

    private Integer count;

    private Integer click;

    private String coverImg;
    
    private List<CourseCategoryResp> child;

    private List<Course> courseList;
}
