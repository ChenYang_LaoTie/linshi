package com.mindspore.mindsporemanagebackend.controller;

import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.pojo.Dissertation;
import com.mindspore.mindsporemanagebackend.pojo.DissertationCategory;
import com.mindspore.mindsporemanagebackend.pojo.DissertationSearch;
import com.mindspore.mindsporemanagebackend.service.DissertationService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/source")
public class DissertationController {

    @Autowired
    private DissertationService dissertationService;

    @RequestMapping(path = "/dissertation/list", method = RequestMethod.POST)
    public PageObject<Dissertation> getDissertationList(@RequestBody DissertationSearch search) {
        return dissertationService.getDissertationList(search);
    }

    @RequestMapping(path = "/dissertation/add", method = RequestMethod.POST)
    public SysResult insertDissertation(Dissertation dissertation) {
        dissertationService.insertDissertation(dissertation);
        return SysResult.ok();
    }

    @RequestMapping(path = "/dissertation/update", method = RequestMethod.POST)
    public SysResult updateDissertation(Dissertation dissertation) {
        dissertationService.updateDissertation(dissertation);
        return SysResult.ok();
    }

    @RequestMapping(path = "/dissertation/release/{id}", method = RequestMethod.GET)
    public SysResult releaseDissertation(@PathVariable("id") Integer id) {
        dissertationService.releaseOrUnReleaseDissertation(true, id);
        return SysResult.ok();
    }

    @RequestMapping(path = "/dissertation/unrelease/{id}", method = RequestMethod.GET)
    public SysResult unReleaseDissertation(@PathVariable("id") Integer id) {
        dissertationService.releaseOrUnReleaseDissertation(false, id);
        return SysResult.ok();
    }

    @RequestMapping(path = "/dissertation/delete", method = RequestMethod.GET)
    public SysResult deleteDissertation(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要删除的论文", null);
        }
        dissertationService.deleteDissertation(ids);
        return SysResult.ok();
    }

    @RequestMapping(path = "/DissertationCategory/uploadFile", method = RequestMethod.POST)
    public FileVo uploadImageFile(MultipartFile uploadFile) {
        return dissertationService.uploadImageFile(uploadFile, StringConstants.POPULAR_RECOMMENDATION_COVER);
    }

    @RequestMapping(path = "/DissertationCategory/add", method = RequestMethod.POST)
    public SysResult insertDissertationCategory(DissertationCategory dissertation) {
        dissertationService.insertDissertationCategory(dissertation);
        return SysResult.ok();
    }

    @RequestMapping(path = "/DissertationCategory/update", method = RequestMethod.POST)
    public SysResult updateDissertationCategory(DissertationCategory dissertation) {
        dissertationService.updateDissertationCategory(dissertation);
        return SysResult.ok();
    }

    @RequestMapping(path = "/DissertationCategory/delete", method = RequestMethod.GET)
    public SysResult deleteDissertationCategory(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要删除的类目", null);
        }
        String result = dissertationService.deleteDissertationCategory(id);
        if (result != null) {
            return SysResult.fail(result, null);
        } else {
            return SysResult.ok();
        }
    }

    @RequestMapping(path = "/DissertationCategory/list", method = RequestMethod.GET)
    public SysResult getDissertationCategoryList(String lang) {
        return SysResult.ok(dissertationService.getDissertationCategoryList(lang));
    }
}
