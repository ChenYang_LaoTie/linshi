package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Cases;
import com.mindspore.mindsporemanagebackend.pojo.CasesCategory;
import com.mindspore.mindsporemanagebackend.pojo.response.CasesCategoryResp;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CasesService {

    List<CasesCategoryResp> getCasesCategoryDropDown();

    void addCasesCategory(CasesCategory casesCategory);

    CasesCategory findCasesCategory(Integer id);

    String deleteCasesCategory(Integer id);

    FileVo upload(MultipartFile uploadFile, String type);

    void addCases(Cases cases);

    void updateCases(Cases cases);

    PageObject<Cases> getCasesList(Integer pageCurrent, Integer limit, String name, Integer categoryId);

    String releaseCases(Integer[] ids);

    String unshelfCases(Integer[] ids);

    String deleteCases(Integer[] ids);
}
