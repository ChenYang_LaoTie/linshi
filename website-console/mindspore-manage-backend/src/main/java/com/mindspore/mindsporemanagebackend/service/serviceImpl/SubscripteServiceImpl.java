package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import com.mindspore.mindsporemanagebackend.mapper.SubscripteMapper;
import com.mindspore.mindsporemanagebackend.pojo.Email;
import com.mindspore.mindsporemanagebackend.service.SubscripeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubscripteServiceImpl implements SubscripeService {

    @Autowired
    private SubscripteMapper subscripteMapper;

    @Override
    public List<Email> selectEmailByLang(String lang) {
        return subscripteMapper.selectEmailByLang(lang);
    }

    @Override
    public List<Email> push() {
        return subscripteMapper.selectEmail();
    }

    @Override
    public Integer countSubscription() {
        return subscripteMapper.countSubscription();
    }

    @Override
    public Integer countCancelSubscription() {
        return subscripteMapper.countCancelSubscription();
    }

}
