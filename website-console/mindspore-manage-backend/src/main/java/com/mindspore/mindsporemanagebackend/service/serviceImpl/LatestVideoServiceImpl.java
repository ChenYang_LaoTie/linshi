package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;
import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.mapper.LatestVideoMapper;
import com.mindspore.mindsporemanagebackend.pojo.LatestVideo;
import com.mindspore.mindsporemanagebackend.service.LatestVideoService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LatestVideoServiceImpl implements LatestVideoService {

    @Autowired
    private LatestVideoMapper latestVideoMapper;

    @Override
    public FileVo upload(MultipartFile uploadFile) {
        FileVo fileVo = new FileVo();

        // 1.获取文件名称,adc/jpg/JPG
        String fileName = uploadFile.getOriginalFilename();
        // 2.将文件名称统统小写,方便判断
        fileName = fileName.toLowerCase();
        // 3.利用正则表达式判断
        if (!fileName.matches("^.+\\.(png|jpg|gif|webp|jpeg)$")) {
            // 表示文件类型不匹配
            fileVo.setErrno(1);
            return fileVo;
        }
        // 4.判断是否为恶意程序
        try {
            boolean isGif = fileName.toUpperCase().endsWith(".GIF");
            int height = 0;
            int width = 0;
            if (!isGif) {
                BufferedImage image = ImageIO.read(uploadFile.getInputStream());
                // 4.1获取宽带和高度
                height = image.getHeight();
                width = image.getWidth();
                // 4.2判断属性是否为0
                if (width == 0 || height == 0) {
                    fileVo.setErrno(1);
                    return fileVo;
                }
            }
            // 5.根据时间生成文件夹
            String dateDir = StringConstants.VIDEO;
            String localDir = String.format("%s%s", PathConstants.imageDirPath, dateDir);
            File dirFile = new File(localDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            // 6.防止文件名重复
            // 6.1生成UUID
            String uuidName = UUID.randomUUID().toString().replaceAll("-", "");
            // 6.2获取文件类型.进行拼接
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            String realName = uuidName + fileType;
            // 6.3实现文件上传
            File realFile = new File(localDir + File.separator + realName);
            uploadFile.transferTo(realFile);
            fileVo.setHeight(height);
            fileVo.setWigth(width);
            // 设置图片虚拟访问路径
            String realurlPath = String.format("%s%s/%s", PathConstants.imageAccessPath, dateDir,realName);
            fileVo.setUrl(realurlPath);
        } catch (IOException e) {
            log.error(e.getMessage());
            fileVo.setErrno(1);// 表示为恶意程序
            return fileVo;
        }
        return fileVo;
    }

    @Override
    public PageObject<LatestVideo> selectLatestVideo(Integer pageCurrent, Integer limit) {
        PageObject<LatestVideo> pageObject = new PageObject<>();
        // 对参数进行校验
        if (pageCurrent == null || pageCurrent < 1) {
            throw new ServiceException("pageCurrent param error");
        }
        // 查找总记录数,并进行校验
        int rowCount = latestVideoMapper.getRowCount();
        if (rowCount == 0) {
            return pageObject;
        }
        Map<String, Integer> map = new HashMap<>(4);
        map.put("totalNum", rowCount);
        int startIndex = (pageCurrent - 1) * limit;
        List<LatestVideo> records = latestVideoMapper.selectLatestVideo(startIndex, limit);
        pageObject.setPage(pageCurrent).setTotalNum(map).setRecords(records).setSize(limit);
        return pageObject;
    }

    @Override
    public LatestVideo getLatestVideoById(Integer id) {
        return latestVideoMapper.getLatestVideoById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertLatestVideo(LatestVideo video) {
        latestVideoMapper.insertLatestVideo(video);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateLatestVideoById(LatestVideo video) {
        latestVideoMapper.updateLatestVideoById(video);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteLatestVideo(Integer... ids) {
        latestVideoMapper.deleteLatestVideo(ids);
    }

    @Override
    public List<LatestVideo> getLatestvideoList(String lang) {
        return latestVideoMapper.getLatestvideoList(lang);
    }

    @Override
    public List<LatestVideo> getLatestvideoOneLevelList(String lang) {
        return latestVideoMapper.getLatestvideoOneLevelList(lang);
    }

}
