package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
//import org.thymeleaf.TemplateEngine;
//import org.thymeleaf.context.Context;

import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.mapper.InfoPushMapper;
import com.mindspore.mindsporemanagebackend.pojo.Information;
import com.mindspore.mindsporemanagebackend.service.InfoPushService;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class InfoPushServiceImpl implements InfoPushService {

    //发送邮件的用户名
    @Value("${spring.mail.sendername}")
    private String sender;

    @Autowired
    private InfoPushMapper infoPushMapper;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;


    @Override
    public void saveInfo(Information info) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        info.setCreatedTime(df.format(new Date()));
        info.setStatus(0);
        infoPushMapper.saveInfo(info);
    }

    @Override
    public PageObject<Information> selectInfo(Integer pageCurrent, Integer limit) {
        PageObject<Information> pageObject = new PageObject<>();
        //对参数进行校验
        if (pageCurrent == null || pageCurrent < 1) {
            throw new ServiceException("当前页码值不正确");
        }
        //查找总记录数,并进行校验
        int rowCount = infoPushMapper.getRowCount();
        if (rowCount == 0) {
            return pageObject;
        }
        Map<String, Integer> map = new HashMap<>(4);
        map.put("totalNum", rowCount);
        int startIndex = (pageCurrent - 1) * limit;
        List<Information> records = infoPushMapper.selectInfo(startIndex, limit);
        pageObject.setPage(pageCurrent).setTotalNum(map).setRecords(records).setSize(limit);
        return pageObject;
    }

    @Override
    public List<Information> selectInfoById(Integer... ids) {
        return infoPushMapper.selectInfoById(ids);
    }

    @Override
    public void deleteInfo(Integer... ids) {
        infoPushMapper.deleteInfo(ids);
    }

    @Override
    public void updateInfo(Information info) {
        infoPushMapper.updateInfo(info);
    }

    @Override
    public void pushInfoEmail(String subject, String email, List<Information> list, String address, String language) throws Exception {
        MimeMessage message = javaMailSender.createMimeMessage();
        //生成模板对象
        Context context = new Context();
        context.setVariable("InfoList", list);
        context.setVariable("emailInfo", address);
        context.setVariable("language", language);
        String emailText = templateEngine.process("info", context);
        //消息处理助手对象
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        //设置发件人
        helper.setFrom(sender);
        //设置收件人
        if (StringUtils.hasText(address)) {
            helper.setTo(address);
            helper.setText(emailText, true);
            helper.setSubject(subject);
            javaMailSender.send(message);
            System.out.println("email to " + address + " -success!!!!!!");
        }
        if (StringUtils.hasText(email)) {
            String[] splitEmail = email.split(",");
            for (String s : splitEmail) {
                helper.setTo(s);
                //设置邮件内容,true表示发送html格式
                helper.setText(emailText, true);
                helper.setSubject(subject);
                javaMailSender.send(message);
            }
        }
    }

    @Override
    public void updateInfoAfterSend(Information info) {
        info.setStatus(1);
        infoPushMapper.updateInfoAfterSend(info);
    }
}
