package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.ClickVideo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClickVideoMapper {

    int countSelectAllName();

    List<ClickVideo> selectAllNameByPage(@Param("start") Integer start, @Param("limit") Integer limit);

    List<ClickVideo> selectFromCountsByNameOnly(@Param("videoName") String name);

    int selectIpCountsByNameOnly(@Param("videoName") String name);

    List<ClickVideo> selectAllNameByTime(@Param("beginTime") String beginTime, @Param("endTime") String endTime);

    List<ClickVideo> selectFromCountsByName(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("videoName") String name);

    int selectIpCountsByName(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("videoName") String name);
}
