package com.mindspore.mindsporemanagebackend.controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.pojo.LatestVideo;
import com.mindspore.mindsporemanagebackend.service.LatestVideoService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;

@RestController
@RequestMapping("/source")
public class LatestVideoController {

    @Autowired
    private LatestVideoService latestVideoService;

    @RequestMapping(path = "/latestvideo/uploadFile", method = RequestMethod.POST)
    public FileVo uploadFile(MultipartFile uploadFile) {
        return latestVideoService.upload(uploadFile);
    }

    @RequestMapping(path = "/latestvideo/list", method = RequestMethod.GET)
    public PageObject<LatestVideo> selectLatestVideo(Integer pageCurrent, Integer limit) {
        return latestVideoService.selectLatestVideo(pageCurrent, limit);
    }

    @RequestMapping(path = "/latestvideo/lang/list", method = RequestMethod.GET)
    public SysResult getLatestvideoList(@RequestParam("lang") String lang) {
        List<LatestVideo> oneList = latestVideoService.getLatestvideoOneLevelList(lang);
        List<LatestVideo> twoList = latestVideoService.getLatestvideoList(lang);
        HashMap<String, LinkedList<LatestVideo>> innerMap = new HashMap<>();
        LinkedList<LatestVideo> list = null;
        for (LatestVideo video : twoList) {
            if (innerMap.get(video.getSupercode()) == null) {
                list = new LinkedList<LatestVideo>();
                list.add(video);
                innerMap.put(video.getSupercode(), list);
            } else {
                list = innerMap.get(video.getSupercode());
                list.add(video);
                innerMap.put(video.getSupercode(), list);
            }
        }
        for (int i = 0; i < oneList.size(); i++) {
            oneList.get(i).setChildList(innerMap.get(oneList.get(i).getCode()));
        }
        return SysResult.ok("success", oneList);
    }

    @RequestMapping(path = "/latestvideo/getById", method = RequestMethod.GET)
    public SysResult getLatestVideoById(@RequestParam("id") Integer id) {
        return SysResult.ok(latestVideoService.getLatestVideoById(id));
    }

    @RequestMapping(path = "/latestvideo/insert", method = RequestMethod.POST)
    public SysResult insertLatestVideo(LatestVideo latestVideo) {
        if (latestVideo.getTitle().contains("&amp;")) {
            latestVideo.setTitle(latestVideo.getTitle().replace("amp;", ""));
        }
        if (latestVideo.getLink().contains("&amp;")) {
            latestVideo.setLink(latestVideo.getLink().replace("amp;", ""));
        }
        latestVideoService.insertLatestVideo(latestVideo);
        return SysResult.ok();
    }

    @RequestMapping(path = "/latestvideo/update", method = RequestMethod.POST)
    public SysResult updateLatestVideoById(LatestVideo latestVideo) {
        if (latestVideo.getTitle().contains("&amp;")) {
            latestVideo.setTitle(latestVideo.getTitle().replace("amp;", ""));
        }
        if (latestVideo.getLink().contains("&amp;")) {
            latestVideo.setLink(latestVideo.getLink().replace("amp;", ""));
        }
        latestVideoService.updateLatestVideoById(latestVideo);
        return SysResult.ok();
    }

    @RequestMapping(path = "/latestvideo/delete", method = RequestMethod.GET)
    public SysResult deleteLatestVideo(Integer... ids) {
        if (ids == null) {
            throw new ServiceException("请先选择要删除的视频");
        }
        latestVideoService.deleteLatestVideo(ids);
        return SysResult.ok();
    }

}
