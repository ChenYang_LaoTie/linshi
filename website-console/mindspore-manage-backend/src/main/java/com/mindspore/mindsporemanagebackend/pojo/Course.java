package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator 课程
 *
 */
@Data
public class Course implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4004055001464045246L;
    private Integer id;

    /**
     * 课程分类编号
     */
    private String categoryId;

    /**
     * 课程标题
     */
    private String title;
    /**
     * 课程标签
     */
    private String label;

    /**
     * 语言
     */
    private String lang;

//    /**
//     * 推荐课程
//     */
//    private String recommendCourses;

    /**
     * 课程形式 视频 1 文档 2 链接3
     */
    private Integer form;

    /**
     * 视频类型 1普通视频 2上传OBS桶链接 3 上传B站视频链接 0不区分
     */
    private Integer videoType;

    /**
     * 视频路径 多个使用分号分隔
     */
    private String videoUrl;
    /**
     * 播放时间
     */
    private Integer playMin;

    /**
     * 状态 0 未上架 1已上架
     */
    private String status;

    /**
     * 上架日期
     */
    private String releaseDate;

    /**
     * 课程序号
     */
    private Integer sequenceNo;
    /**
     * 学习量
     */
    private Integer clicks;
    private String updateTime;

    /**
     * 文档详情
     */
    private String document;
}