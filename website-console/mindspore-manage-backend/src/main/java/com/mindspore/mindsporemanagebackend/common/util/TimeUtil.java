package com.mindspore.mindsporemanagebackend.common.util;

import java.text.SimpleDateFormat;

public class TimeUtil {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static String getDate(Long time) {
        return null == time ? null : simpleDateFormat.format(time);
    }

    public static String getShortDate(Long time) {
        return null == time ? DateFormat.format(System.currentTimeMillis()) : DateFormat.format(time);
    }
}
