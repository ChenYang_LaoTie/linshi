package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 模型管理
 * 
 * @author Administrator
 *
 */
@Data
public class Modeled implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8529911972364788977L;
    private Integer id;
    /**
     * 示例标题
     */
    private String title;
    /**
     * 示例icon
     */
    private String icon;
    /**
     * 介绍
     */
    private String introduce;
    /**
     * 模型链接
     */
    private String link;
    /**
     * 发布日期
     */
    private String releaseDate;
    /**
     * 中英文
     */
    private String lang;
    /**
     * 序号
     */
    private Integer sequenceNo;
    /**
     * 状态 0 未上架 1已上架
     */
    private String status;

    private String updateTime;

}
