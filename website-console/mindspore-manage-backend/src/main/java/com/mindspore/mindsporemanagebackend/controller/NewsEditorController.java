package com.mindspore.mindsporemanagebackend.controller;

import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.pojo.MsNews;
import com.mindspore.mindsporemanagebackend.pojo.NewsDetail;
import com.mindspore.mindsporemanagebackend.service.NewsEditorService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/source")
public class NewsEditorController {

    @Autowired
    private NewsEditorService newsEditorService;

    @RequestMapping(path = "/uploadFile", method = RequestMethod.POST)
    public FileVo uploadFile(MultipartFile uploadFile, String tag) {
        return newsEditorService.upload(uploadFile, tag);
    }

    @RequestMapping(path = "/selectNews", method = RequestMethod.GET)
    public PageObject<MsNews> selectNews(Integer pageCurrent, Integer limit, Integer type, String newsTitle, String tag) {
        // 对参数进行校验
        if (pageCurrent == null || pageCurrent < 1) {
            throw new ServiceException("当前页码值不正确");
        }

        if (type == null) {
            type = 0;
        }

        if (!StringUtils.hasText(tag)) {
            tag = "zh";
        }

        return newsEditorService.selectMindNews(pageCurrent, limit, type, newsTitle, tag);
    }

    @RequestMapping(path = "/insertNews", method = RequestMethod.POST)
    public SysResult insertNews(MsNews news, NewsDetail detail) {
        if (news.getNewsTitle().contains("&amp;")) {
            news.setNewsTitle(news.getNewsTitle().replace("amp;", ""));
        }
        if (news.getNewsContent().contains("&amp;")) {
            news.setNewsContent(news.getNewsContent().replace("amp;", ""));
        }
        newsEditorService.insertNews(news, detail);
        return SysResult.ok();
    }

    @RequestMapping(path = "/deleteNews", method = RequestMethod.GET)
    public SysResult deleteNews(Integer... ids) {
        if (ids == null) {
            throw new ServiceException("请先选择要删除的新闻");
        }
        newsEditorService.deleteWebNews(ids);
        return SysResult.ok();
    }

    @RequestMapping(path = "/selectNewsById", method = RequestMethod.GET)
    public SysResult selectNewsById(Integer id) {
        MsNews news = newsEditorService.selectNewsById(id);
        return SysResult.ok("查询成功", news);
    }

    // 更新新闻 ok
    @RequestMapping(path = "/updateNews", method = RequestMethod.POST)
    public SysResult updateNewsById(MsNews news, NewsDetail detail) {
        if (news.getNewsTitle().contains("&amp;")) {
            news.setNewsTitle(news.getNewsTitle().replace("amp;", ""));
        }
        if (news.getNewsContent().contains("&amp;")) {
            news.setNewsContent(news.getNewsContent().replace("amp;", ""));
        }
        newsEditorService.updateNewsById(news, detail);
        return SysResult.ok();
    }

    @RequestMapping(path = "/selectNewsdesc", method = RequestMethod.GET)
    public SysResult selectNewsDescById(Integer id) {
        NewsDetail detail = newsEditorService.selectNewsDescById(id);
        return SysResult.ok("执行成功", detail);
    }

    @RequestMapping(path = "/updateNewsDesc", method = RequestMethod.GET)
    public SysResult updateNewsDesc(NewsDetail detail) {
        newsEditorService.updateNewsDescById(detail);
        return SysResult.ok("更新成功");
    }

    @RequestMapping(path = "/releaseNews", method = RequestMethod.GET)
    public SysResult releaseNews(Integer... ids) {
        newsEditorService.releaseNews(ids);
        return SysResult.ok();
    }
}
