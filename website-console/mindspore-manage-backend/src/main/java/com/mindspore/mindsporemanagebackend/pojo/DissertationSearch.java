package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class DissertationSearch implements Serializable {
    private static final long serialVersionUID = 6941102146510665331L;
    /**
     * 领域 1-图像 2-文本 3-音频 4-推荐 9-其他
     */
    private Integer domain;
    private String[] domainArr;
    /**
     * 发表单位
     */
    private String publishedBy;
    private String[] publishedByArr;
    /**
     * 发表时间
     */
    private String postedOnStart;
    private String postedOnEnd;
    /**
     * 来源
     */
    private Integer sources;
    private String[] sourcesArr;
    /**
     * 语言
     */
    private String lang;
    private Integer page;
    private int startIndex;
    private int pageSize = 5;
    
    private Integer mindspore;
}
