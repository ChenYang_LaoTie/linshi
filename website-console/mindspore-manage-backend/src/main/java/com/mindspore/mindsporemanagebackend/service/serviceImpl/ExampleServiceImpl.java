package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;
import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.common.util.TimeUtil;
import com.mindspore.mindsporemanagebackend.mapper.ExampleMapper;
import com.mindspore.mindsporemanagebackend.pojo.Example;
import com.mindspore.mindsporemanagebackend.service.ExampleService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExampleServiceImpl implements ExampleService {


    @Autowired
    private ExampleMapper exampleMapper;

    @Override
    public FileVo upload(MultipartFile uploadFile, String type) {
        FileVo fileVo = new FileVo();
        // 1.获取文件名称,adc/jpg/JPG
        String fileName = uploadFile.getOriginalFilename();
        fileVo.setTrueName(fileName);
        if (type.equals(StringConstants.EXAMPLE_ICON)) {
            // 3.利用正则表达式判断
            if (!fileName.matches("^.+\\.(png|icon|PNG|ICON|ico|ICO)$")) {
                // 表示文件类型不匹配
                fileVo.setTag("类型不匹配");
                fileVo.setErrno(1);
                return fileVo;
            }
        }
        // 4.判断是否为恶意程序
        try {
            int height = 0;
            int width = 0;
            // 5.根据时间生成文件夹
            String localDir = String.format("%s%s/%s", PathConstants.imageDirPath, StringConstants.EXAMPLE, type);
            File dirFile = new File(localDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            // 6.防止文件名重复
            // 6.1生成UUID
            String uuidName = UUID.randomUUID().toString().replaceAll("-", "");
            // 6.2获取文件类型.进行拼接
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            String realName = uuidName + fileType;
            // 6.3实现文件上传
            File realFile = new File(localDir + File.separator + realName);
            uploadFile.transferTo(realFile);
            fileVo.setHeight(height);
            fileVo.setWigth(width);
            // 设置图片虚拟访问路径
            String realurlPath = String.format("%s%s/%s/%s", PathConstants.imageAccessPath, StringConstants.EXAMPLE, type, realName);
            fileVo.setUrl(realurlPath);
            fileVo.setSize(FileUtils.byteCountToDisplaySize(realFile.length()));
        } catch (IOException e) {
            log.error(e.getMessage());
            fileVo.setErrno(1);// 表示为恶意程序
            return fileVo;
        }
        return fileVo;
    }

    @Override
    public void addOrUpdateExample(Example example) {
        if (example.getId() == null || example.getId() == 0) {
            example.setStatus("0");
        }
        if (example.getSequenceNo() == null || example.getSequenceNo() == 0) {
            example.setSequenceNo(999);
        }
        example.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        exampleMapper.addOrUpdateExample(example);
    }

    @Override
    public PageObject<Example> getExampleList(Integer pageCurrent, Integer limit) {
        PageObject<Example> pageObject = new PageObject<>();
        // 对参数进行校验
        if (pageCurrent == null || pageCurrent < 1) {
            throw new ServiceException("当前页码值不正确");
        }
        // 查找总记录数,并进行校验
        int rowCount = exampleMapper.getExampleCount();
        if (rowCount == 0) {
            return pageObject;
        }
        Map<String, Integer> map = new HashMap<>(4);
        map.put("totalNum", rowCount);
        int startIndex = (pageCurrent - 1) * limit;
        List<Example> records = exampleMapper.getExampleList(startIndex, limit);
        pageObject.setPage(pageCurrent).setTotalNum(map).setRecords(records).setSize(limit);
        return pageObject;
    }

    @Override
    public String releaseExample(Integer[] ids) {
        exampleMapper.releaseExample(ids);
        return null;
    }

    @Override
    public String unshelfExample(Integer[] ids) {
        exampleMapper.unshelfExample(ids);
        return null;
    }

    @Override
    public String deleteExample(Integer[] ids) {
        exampleMapper.deleteExampleByIds(ids);
        return null;
    }


}
