package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Resume;

import java.util.List;

public interface ResumeService {

    List<Resume> queryResumes(String start, String end, String name, String phone);

}
