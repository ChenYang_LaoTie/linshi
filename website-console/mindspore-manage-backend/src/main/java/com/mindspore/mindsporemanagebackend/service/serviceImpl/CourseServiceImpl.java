package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson2.JSON;
import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;
import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.common.util.TimeUtil;
import com.mindspore.mindsporemanagebackend.mapper.CourseMapper;
import com.mindspore.mindsporemanagebackend.pojo.*;
import com.mindspore.mindsporemanagebackend.pojo.enums.CourseStatus;
import com.mindspore.mindsporemanagebackend.pojo.response.CourseCategoryResp;
import com.mindspore.mindsporemanagebackend.service.CourseService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Service
@Slf4j
public class CourseServiceImpl implements CourseService {

    static final String TYPE_ERROR = "类型不匹配";
    static final Integer SEQUENCENO = 999;

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public List<CoursePopular> getCoursePopularList(String lang) {
        return courseMapper.getCoursePopularList(lang);
    }

    @Override
    public List<CourseCategoryResp> getCourseCategoryDropDown(String lang) {
        // 查询结果
        List<CourseCategory> allList = courseMapper.getCourseCategoryList(lang);

        // 返回列表、课程目录与父节点对应关系
        List<CourseCategoryResp> result = new LinkedList<>();
        Map<Integer, LinkedList<CourseCategoryResp>> map = new LinkedHashMap<>(16);

        // 构造一级课程目录、生成课程目录与父节点对应关系
        CourseCategoryResp resp = null;
        for (CourseCategory category : allList) {
            resp = new CourseCategoryResp();
            resp.setId(category.getId());
            resp.setName(category.getCatalog());
            if (category.getSuperId() == null) {
                result.add(resp);
            } else {
                LinkedList<CourseCategoryResp> list = map.get(category.getSuperId());
                if (list == null) {
                    list = new LinkedList<>();
                }
                list.add(resp);
                map.put(category.getSuperId(), list);
            }
        }

        // 补充一级课程目录下的子目录
        if (result.size() > 0) {
            for (int i = 0; i < result.size(); i++) {
                LinkedList<CourseCategoryResp> innerList = map.get(result.get(i).getId());
                if (innerList == null) {
                    continue;
                }
                for (int a = 0; a < innerList.size(); a++) {
                    innerList.get(a).setChild(map.get(innerList.get(a).getId()));
                }
                result.get(i).setChild(innerList);
            }
        }

        return result;
    }

    @Override
    public FileVo upload(MultipartFile uploadFile, String type) {
        FileVo fileVo = new FileVo();

        // 1.获取文件名称,adc/jpg/JPG
        String fileName = uploadFile.getOriginalFilename();
        fileVo.setTrueName(fileName);
        if (type.equals(StringConstants.COURSE_CATEGORY_COVER) || type.equals(StringConstants.COURSE_INSTRUCTOR_AVATAR)
                || type.equals(StringConstants.COURSE_POPULAR)
                || type.equals(StringConstants.COURSE_IMAGE)) {
            // 3.利用正则表达式判断
            if (!fileName.matches("^.+\\.(png|jpg|PNG|JPG|jpeg|JPEG)$")) {
                // 表示文件类型不匹配
                fileVo.setTag(TYPE_ERROR);
                fileVo.setErrno(1);
                return fileVo;
            }
        } else if (type.equals(StringConstants.COURSE_VIDEO)) {
            // 3.利用正则表达式判断
            if (!fileName.matches("^.+\\.(mp4|MP4)$")) {
                // 表示文件类型不匹配
                fileVo.setTag(TYPE_ERROR);
                fileVo.setErrno(1);
                return fileVo;
            }
        } else if (type.equals(StringConstants.COURSE_DOCUMENT)) {
            // 3.利用正则表达式判断
            if (!fileName.matches("^.+\\.(ppt|pptx|PPT|PPTX|pdf|PDF)$")) {
                // 表示文件类型不匹配
                fileVo.setTag(TYPE_ERROR);
                fileVo.setErrno(1);
                return fileVo;
            }
        }
        // 4.判断是否为恶意程序
        try {
            boolean isGif = fileName.toUpperCase().endsWith(".GIF");
            int height = 0;
            int width = 0;
            if (!isGif) {
                if (type.equals(StringConstants.COURSE_CATEGORY_COVER)
                        || type.equals(StringConstants.COURSE_INSTRUCTOR_AVATAR)
                        || type.equals(StringConstants.COURSE_POPULAR)
                        || type.equals(StringConstants.COURSE_IMAGE)) {
                    BufferedImage image = ImageIO.read(uploadFile.getInputStream());
                    // 4.1获取宽带和高度
                    height = image.getHeight();
                    width = image.getWidth();
                    // 4.2判断属性是否为0
                    if (width == 0 || height == 0) {
                        fileVo.setErrno(1);
                        return fileVo;
                    }
                }
            }
            // 5.根据时间生成文件夹
            String localDir = String.format("%s%s/%s", PathConstants.imageDirPath, StringConstants.COURSE, type);
            File dirFile = new File(localDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            // 6.防止文件名重复
            // 6.1生成UUID
            String uuidName = UUID.randomUUID().toString().replaceAll("-", "");
            // 6.2获取文件类型.进行拼接
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            String realName = uuidName + fileType;
            // 6.3实现文件上传
            File realFile = new File(localDir + File.separator + realName);
            uploadFile.transferTo(realFile);
            fileVo.setHeight(height);
            fileVo.setWigth(width);
            // 设置图片虚拟访问路径
            String realurlPath = String.format("%s%s/%s/%s", PathConstants.imageAccessPath, StringConstants.COURSE,type, realName);
            fileVo.setUrl(realurlPath);
            fileVo.setSize(FileUtils.byteCountToDisplaySize(realFile.length()));
        } catch (IOException e) {
            log.error(e.getMessage());
            fileVo.setErrno(1);// 表示为恶意程序
            return fileVo;
        }
        return fileVo;
    }

    @Override
    public void addCoursePopular(CoursePopular coursepopular) {
        if (coursepopular.getSequence() == null || coursepopular.getSequence() == 0) {
            coursepopular.setSequence(SEQUENCENO);
        }

        if (coursepopular.getId() == null || coursepopular.getId() == 0) {
            coursepopular.setCreateTime(TimeUtil.getDate(System.currentTimeMillis()));
        }
        coursepopular.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        courseMapper.saveOrUpdateCoursePopular(coursepopular);
    }

    @Override
    public String deleteCoursePopular(Integer id) {
        courseMapper.deleteCoursePopular(id);
        return null;
    }

    @Override
    public void addCourseCategory(CourseCategory courseCategory) {
        if (courseCategory != null && !StringUtils.isEmpty(courseCategory.getCatalog())) {
            if (courseCategory.getSequenceNo() == null) {
                courseCategory.setSequenceNo(SEQUENCENO);
            }
            if (!StringUtils.isEmpty(courseCategory.getTeamId())) {
                courseCategory.setTeamId(courseCategory.getTeamId().replace("，", ","));
                if (!courseCategory.getTeamId().endsWith(",")) {
                    courseCategory.setTeamId(courseCategory.getTeamId() + ",");
                }
            }
            courseCategory.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
            courseMapper.saveOrUpdateCourseCategory(courseCategory);
        }
    }

    @Override
    public CourseCategory findCourseCategory(Integer id) {
        return courseMapper.findCourseCategoryById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String deleteCourseCategory(Integer id) {
        String result = null;
        CourseCategory category = courseMapper.findCourseCategoryById(id);
        if (category.getLevel().intValue() == 1 || category.getLevel().intValue() == 2) {
            List<CourseCategory> list = courseMapper.getCourseCategoryListBySuperId(id);
            if (list != null && list.size() > 0) {
                return "已有下级分类";
            }
        }
        List<Course> courseList = courseMapper.getCourseListByCategoryId(category.getId());
        if (courseList != null && courseList.size() > 0) {
            result = "已有课程";
        } else {
            courseMapper.deleteCourseCategoryByIds(category.getId());
        }
        return result;
    }

    @Override
    public void addCourseInstructor(CourseInstructor instructor) {
        if (instructor != null && !StringUtils.isEmpty(instructor.getName())) {
            instructor.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
            courseMapper.saveOrUpdateCourseInstructor(instructor);
        }
    }

    @Override
    public String deleteCourseInstructor(Integer id) {
        List<CourseCategory> list = courseMapper.getCourseCategoryListLikeTeamId("%" + id + ",%");
        if (list != null && list.size() > 0) {
            return "已关联课程目录，禁止删除";
        } else {
            courseMapper.deleteCourseInstructorById(id);
            return null;
        }
    }

    @Override
    public List<CourseInstructor> searchCourseInstructor(String name) {
        if (StringUtils.isEmpty(name)) {
            return courseMapper.getCourseInstructor();
        }
        return courseMapper.searchCourseInstructor(name);
    }

    @Override
    public PageObject<CourseInstructor> getCourseInstructorList(Integer pageCurrent, Integer limit, String name) {
        PageObject<CourseInstructor> pageObject = new PageObject<>();
        // 对参数进行校验
        if (pageCurrent == null || pageCurrent < 1) {
            throw new ServiceException("当前页码值不正确");
        }
        // 查找总记录数,并进行校验
        int rowCount = courseMapper.getCourseInstructorCount(name);
        if (rowCount == 0) {
            return pageObject;
        }
        Map<String, Integer> map = new HashMap<>();
        map.put("totalNum", rowCount);
        int startIndex = (pageCurrent - 1) * limit;
        List<CourseInstructor> records = courseMapper.getCourseInstructorList(startIndex, limit, name);
        // 查询讲师所有课程
        if (records != null && records.size() > 0) {
            List<CourseCategory> list = courseMapper.getCourseCategoryListByLevel(3);
            HashMap<String, String> courseMap = new HashMap<String, String>();
            if (list != null && list.size() > 0) {
                for (CourseCategory cc : list) {
                    if (StringUtils.isEmpty(cc.getTeamId())) {
                        continue;
                    }
                    List<String> teamIdList = Arrays.asList(cc.getTeamId().split(","));
                    for (String teamId : teamIdList) {
                        if (courseMap.get(teamId) != null) {
                            courseMap.put(teamId, courseMap.get(teamId) + System.lineSeparator() + cc.getCatalog());
                        } else {
                            courseMap.put(teamId, cc.getCatalog());
                        }
                    }
                }
            }
            for (int i = 0; i < records.size(); i++) {
                records.get(i).setCourse(courseMap.getOrDefault(String.valueOf(records.get(i).getId()), ""));
            }
        }

        pageObject.setPage(pageCurrent).setTotalNum(map).setRecords(records).setSize(limit);
        return pageObject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addCourse(Course course) {
        // 初始化基础属性
        course.setStatus(CourseStatus.NOT_LISTED.getStatus());
        if (course.getSequenceNo() == null || course.getSequenceNo() == 0) {
            course.setSequenceNo(SEQUENCENO);
        }
        if (course.getClicks() == null) {
            course.setClicks(0);
        }
        course.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));

        // 保存课程信息
        int count = courseMapper.saveCourse(course);

        // 同步保存课程文档信息
        if (count > 0 && !StringUtils.isEmpty(course.getDocument())) {
            List<CourseDocument> docList = JSON.parseArray(course.getDocument(), CourseDocument.class);
            if (!CollectionUtils.isEmpty(docList)) {
                for (CourseDocument doc : docList) {
                    courseMapper.saveCourseDocument(course.getId(), doc);
                }
            }
        }
    }

    @Override
    public void updateCourse(Course course) {
        // 更新课程信息
        course.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        if (course.getSequenceNo() == null || course.getSequenceNo() == 0) {
            course.setSequenceNo(SEQUENCENO);
        }
        int count = courseMapper.updateCourse(course);

        // 更新课程文档信息
        if (count > 0 && !StringUtils.isEmpty(course.getDocument())) {
            List<CourseDocument> docList = com.alibaba.fastjson.JSON.parseArray(course.getDocument(), CourseDocument.class);
            if (!CollectionUtils.isEmpty(docList)) {
                courseMapper.deleteCourseDocumentByCourseId(course.getId());
                for (CourseDocument doc : docList) {
                    courseMapper.saveCourseDocument(course.getId(), doc);
                }
            }
        }
    }

    @Override
    public Course getCourseInfo(Integer id) {
        Course course = courseMapper.findCourseById(id);
        List<CourseDocument> documentList = courseMapper.getCourseDocumentByICourseId(id);
        if (documentList != null && documentList.size() > 0) {
            course.setDocument(com.alibaba.fastjson.JSON.toJSONString(documentList));
        }
        return course;
    }

    @Override
    public PageObject<Course> getCourseList(Integer pageCurrent, Integer limit, String name, Integer categoryId, String lang) {
        PageObject<Course> pageObject = new PageObject<>();
        // 查找总记录数,并进行校验
        int rowCount = courseMapper.getCourseCount(name, categoryId, lang);
        if (rowCount == 0) {
            return pageObject;
        }
        Map<String, Integer> map = new HashMap<>();
        map.put("totalNum", rowCount);
        int startIndex = (pageCurrent - 1) * limit;
        List<Course> records = courseMapper.getCourseList(startIndex, limit, name, categoryId, lang);
        pageObject.setPage(pageCurrent).setTotalNum(map).setRecords(records).setSize(limit);
        return pageObject;
    }

    @Override
    public String releaseCourse(Integer[] ids) {
        courseMapper.releaseCourse(ids);
        return null;
    }

    @Override
    public String unshelfCourse(Integer[] ids) {
        courseMapper.unshelfCourse(ids);
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String deleteCourse(Integer[] ids) {
        List<Course> courseList = courseMapper.getCourseByIds(ids);
        if (courseList != null && courseList.size() > 0) {
            for (Course c : courseList) {
                courseMapper.deleteCourseDocumentByCourseId(c.getId());
            }
        }
        courseMapper.deleteCourse(ids);
        return null;
    }


}
