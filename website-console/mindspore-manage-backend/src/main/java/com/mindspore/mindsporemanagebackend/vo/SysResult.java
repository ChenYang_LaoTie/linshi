package com.mindspore.mindsporemanagebackend.vo;

public class SysResult {
    private Integer status;
    private String msg;
    private Object obj;

    public SysResult(Integer status, String msg, Object obj) {
        this.status = status;
        this.msg = msg;
        this.obj = obj;
    }
        public SysResult() {
    }
    public SysResult(Throwable e){
        this.status=201;
        this.msg=e.getMessage();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }



    //定义成功的静态方法
    public static SysResult ok(String msg, Object obj){
        return new SysResult(200,msg,obj);
    }
    //表示定义成功的静态方法
    public static SysResult ok(){
        return new SysResult(200,null,null);
    }
    public static SysResult ok(Object data){
        return new SysResult(200,null,data);
    }
    //定义一个失败的静态方法
    public static SysResult fail(String msg, Object data){
        return new SysResult(201,msg, data);
    }
    public static SysResult fail(){
        return new SysResult(201,null,null);
    }
}

