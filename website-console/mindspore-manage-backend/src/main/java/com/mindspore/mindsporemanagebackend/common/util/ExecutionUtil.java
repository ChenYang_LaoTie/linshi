package com.mindspore.mindsporemanagebackend.common.util;

import com.mindspore.mindsporemanagebackend.common.constant.NumberConstants;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutionUtil {
    private static final ExecutorService executorService = Executors.newFixedThreadPool(NumberConstants.HUNDRED);

    public static void exec(Runnable runnable) {
        ExecutionUtil.executorService.execute(runnable);
    }

    public static void destroy() {
        ExecutionUtil.executorService.shutdownNow();
    }
}
