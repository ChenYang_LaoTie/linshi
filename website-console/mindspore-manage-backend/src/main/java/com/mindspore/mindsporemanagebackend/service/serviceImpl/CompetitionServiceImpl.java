package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;
import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.common.util.TimeUtil;
import com.mindspore.mindsporemanagebackend.mapper.CompetitionMapper;
import com.mindspore.mindsporemanagebackend.pojo.entity.Competition;
import com.mindspore.mindsporemanagebackend.pojo.entity.CompetitionPopular;
import com.mindspore.mindsporemanagebackend.pojo.enums.PublishStatus;
import com.mindspore.mindsporemanagebackend.service.CompetitionService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CompetitionServiceImpl implements CompetitionService {

    static final String TYPE_ERROR = "类型不匹配";
    public static final Integer SEQUENCENO = 999;

    @Autowired
    private CompetitionMapper competitionMapper;

    @Override
    public void saveCompetition(Competition competition) {
        // 新增
        if (competition.getId() == null || competition.getId() == 0) {
            competition.setPublishStatus(PublishStatus.UN_PUBLISHED.getStatus());
            competition.setCreateTime(TimeUtil.getDate(System.currentTimeMillis()));
        }
        competition.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        competitionMapper.saveOrUpdateCompetition(competition);
    }

    @Override
    public String deleteCompetition(Integer[] ids) {
        competitionMapper.deleteCompetition(ids);
        return null;
    }

    @Override
    public String publishCompetition(Integer id) {
        Competition updateParam = new Competition();
        updateParam.setId(id);
        updateParam.setPublishStatus(PublishStatus.PUBLISHED.getStatus());
        updateParam.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        competitionMapper.saveOrUpdateCompetition(updateParam);
        return null;
    }

    @Override
    public String batchPublishCompetition(Integer[] ids) {
        competitionMapper.batchPublishCompetition(ids);
        return null;
    }

    @Override
    public FileVo upload(MultipartFile uploadFile, String type) {
        FileVo fileVo = new FileVo();

        // 1.获取文件名称,adc/jpg/JPG
        String fileName = uploadFile.getOriginalFilename();
        fileVo.setTrueName(fileName);
        if (type.equals(StringConstants.COMPETITION_POPULAR)) {
            // 3.利用正则表达式判断
            if (!fileName.matches("^.+\\.(png|jpg|PNG|JPG|jpeg|JPEG)$")) {
                // 表示文件类型不匹配
                fileVo.setTag(TYPE_ERROR);
                fileVo.setErrno(1);
                return fileVo;
            }
        }
        // 4.判断是否为恶意程序
        try {
            boolean isGif = fileName.toUpperCase().endsWith(".GIF");
            int height = 0;
            int width = 0;
            if (!isGif) {
                if (type.equals(StringConstants.COMPETITION_POPULAR)) {
                    BufferedImage image = ImageIO.read(uploadFile.getInputStream());
                    // 4.1获取宽带和高度
                    height = image.getHeight();
                    width = image.getWidth();
                    // 4.2判断属性是否为0
                    if (width == 0 || height == 0) {
                        fileVo.setErrno(1);
                        return fileVo;
                    }
                }
            }
            // 5.根据时间生成文件夹
            String localDir = String.format("%s%s", PathConstants.imageDirPath, type);
            File dirFile = new File(localDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            // 6.防止文件名重复
            // 6.1生成UUID
            String uuidName = UUID.randomUUID().toString().replaceAll("-", "");
            // 6.2获取文件类型.进行拼接
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            String realName = uuidName + fileType;
            // 6.3实现文件上传
            File realFile = new File(localDir + File.separator + realName);
            uploadFile.transferTo(realFile);
            fileVo.setHeight(height);
            fileVo.setWigth(width);
            // 设置图片虚拟访问路径
            String realurlPath = String.format("%s%s/%s", PathConstants.imageAccessPath, type, realName);
            fileVo.setUrl(realurlPath);
            fileVo.setSize(FileUtils.byteCountToDisplaySize(realFile.length()));
        } catch (IOException e) {
            log.error(e.getMessage());
            // 表示为恶意程序
            fileVo.setErrno(1);
            return fileVo;
        }
        return fileVo;
    }

    @Override
    public void saveCompetitionPopular(CompetitionPopular competitionPopular) {
        if (competitionPopular.getSequence() == null || competitionPopular.getSequence() == 0) {
            competitionPopular.setSequence(SEQUENCENO);
        }

        if (competitionPopular.getId() == null || competitionPopular.getId() == 0) {
            competitionPopular.setCreateTime(TimeUtil.getDate(System.currentTimeMillis()));
        }
        competitionPopular.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
        competitionMapper.saveOrUpdateCompetitionPopular(competitionPopular);
    }

    @Override
    public String deleteCompetitionPopular(Integer id) {
        competitionMapper.deleteCompetitionPopular(id);
        return null;
    }


}
