package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.ClickStatistic;
import com.mindspore.mindsporemanagebackend.vo.PageResult;

import java.util.List;

public interface ClickVideoService {

    PageResult<ClickStatistic> selectStatisticByPage(Integer current, Integer limit);

    List<ClickStatistic> statistic(String beginTime, String endTime);

}
