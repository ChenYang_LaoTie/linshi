package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Modeled;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import org.springframework.web.multipart.MultipartFile;

public interface ModeledService {

    FileVo upload(MultipartFile uploadFile, String modeledIcon);

    void addOrUpdateModeled(Modeled modeled);

    PageObject<Modeled> getModeledList(Integer pageCurrent, Integer limit);

    String releaseModeled(Integer[] ids);

    String unshelfModeled(Integer[] ids);

    String deleteModeled(Integer[] ids);
}
