package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.MsNews;
import com.mindspore.mindsporemanagebackend.pojo.NewsDetail;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import org.springframework.web.multipart.MultipartFile;

public interface NewsEditorService {

    FileVo upload(MultipartFile uploadFile, String tag);

    PageObject<MsNews> selectMindNews(Integer pageCurrent, Integer limit, Integer type, String newsTitle, String tag);

    void insertNews(MsNews news, NewsDetail detail);

    void deleteWebNews(Integer[] ids);

    MsNews selectNewsById(Integer id);

    void updateNewsById(MsNews news, NewsDetail detail);

    NewsDetail selectNewsDescById(Integer id);

    void updateNewsDescById(NewsDetail detail);

    void releaseNews(Integer[] ids);

}
