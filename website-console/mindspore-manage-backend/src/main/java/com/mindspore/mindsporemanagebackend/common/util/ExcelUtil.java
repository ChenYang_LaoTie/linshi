package com.mindspore.mindsporemanagebackend.common.util;

import com.mindspore.mindsporemanagebackend.common.constant.NumberConstants;
import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @Description exportUtils
 * @Author LiuTao
 * @Date 2020-05-13 0:41
 */
public class ExcelUtil {

    private final static Logger logger = LoggerFactory.getLogger(ExcelUtil.class);

    public static final String ORDER_NUMBER = "orderName";

    public static <T> byte[] export(String sheetTitle, String[] title, String[] columns, Collection<T> list) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); HSSFWorkbook wb = new HSSFWorkbook()) { // 创建excel表
            HSSFSheet sheet = wb.createSheet(sheetTitle);
            sheet.setDefaultColumnWidth(20);// 设置默认行宽

            // 表头样式（加粗，水平居中，垂直居中）
            HSSFCellStyle cellStyle = wb.createCellStyle();
            cellStyle.setAlignment(HorizontalAlignment.CENTER);// 水平居中
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中
            // 设置边框样式
            cellStyle.setBorderBottom(BorderStyle.THIN); // 下边框
            cellStyle.setBorderLeft(BorderStyle.THIN);// 左边框
            cellStyle.setBorderTop(BorderStyle.THIN);// 上边框
            cellStyle.setBorderRight(BorderStyle.THIN);// 右边框

            HSSFFont fontStyle = wb.createFont();
            fontStyle.setFontHeightInPoints((short) 12);

            cellStyle.setFont(fontStyle);

            // 标题样式（加粗，垂直居中）
            HSSFCellStyle cellStyle2 = wb.createCellStyle();
            cellStyle2.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中
            cellStyle2.setFont(fontStyle);

            // 设置边框样式
            cellStyle2.setBorderBottom(BorderStyle.THIN); // 下边框
            cellStyle2.setBorderLeft(BorderStyle.THIN);// 左边框
            cellStyle2.setBorderTop(BorderStyle.THIN);// 上边框
            cellStyle2.setBorderRight(BorderStyle.THIN);// 右边框

            // 字段样式（垂直居中）
            HSSFCellStyle cellStyle3 = wb.createCellStyle();
            cellStyle3.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直居中

            // 设置边框样式
            cellStyle3.setBorderBottom(BorderStyle.THIN); // 下边框
            cellStyle3.setBorderLeft(BorderStyle.THIN);// 左边框
            cellStyle3.setBorderTop(BorderStyle.THIN);// 上边框
            cellStyle3.setBorderRight(BorderStyle.THIN);// 右边框

            // 创建表头
            HSSFRow row = sheet.createRow(0);
            row.setHeightInPoints(20);// 行高

            HSSFCell cell = row.createCell(0);
            cell.setCellValue(sheetTitle);
            cell.setCellStyle(cellStyle);

            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, (title.length - 1)));

            // 创建标题
            HSSFRow rowTitle = sheet.createRow(1);
            rowTitle.setHeightInPoints(20);

            HSSFCell hc;
            for (int i = 0; i < title.length; i++) {
                hc = rowTitle.createCell(i);
                hc.setCellValue(title[i]);
                hc.setCellStyle(cellStyle2);
            }

            byte[] result = null;

            // 创建表格数据
            Field[] fields;
            int i = 2;

            List<String> columnsList = Arrays.asList(columns);
            for (T obj : list) {
                fields = obj.getClass().getDeclaredFields();

                HSSFRow rowBody = sheet.createRow(i);
                rowBody.setHeightInPoints(20);

                boolean addOrderNumber = columns.length > NumberConstants.ZERO
                        && ORDER_NUMBER.equals(columns[NumberConstants.ZERO]);
                if (addOrderNumber) {
                    hc = rowBody.createCell(NumberConstants.ZERO);
                    hc.setCellValue(i - NumberConstants.ONE);
                    hc.setCellStyle(cellStyle3);
                }

                for (Field f : fields) {

                    f.setAccessible(true);
                    int index = columnsList.indexOf(f.getName());
                    if (index < NumberConstants.ZERO) {
                        continue;
                    }

                    Object va = f.get(obj);
                    if (null == va) {
                        va = "";
                    }

                    hc = rowBody.createCell(index);
                    hc.setCellValue(va.toString());
                    hc.setCellStyle(cellStyle3);
                }

                i++;
            }
            wb.write(out);
            result = out.toByteArray();
            return result;
        } catch (Exception ex) {
            logger.error("export error", ex);
            throw new ServiceException("export error.");
        }
    }

    public static byte[] export(String sheetName, String[] title, List<Integer> list) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(sheetName);
        HSSFRow rowTitle = sheet.createRow(0);
        HSSFCell cellTitle;
        for (int i = 0; i < title.length; i++) {
            cellTitle = rowTitle.createCell(i);
            cellTitle.setCellValue(title[i]);
        }
        HSSFRow rowValue = sheet.createRow(1);
        HSSFCell cellValue;
        for (int i = 0; i < list.size(); i++) {
            cellValue = rowValue.createCell(i);
            cellValue.setCellValue(list.get(i));
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            workbook.write(out);
            return out.toByteArray();
        } catch (IOException e) {
            logger.error("导出错误");
            throw new ServiceException("export error.");
        } finally {
            try {
                out.close();
                workbook.close();
            } catch (IOException e) {
                logger.error("输出流关闭错误");
            }
        }
    }

    public static <T> byte[] export(String sheetName, String[] headers, String[] columns, List<T> source) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(sheetName);
        HSSFRow rowHead = sheet.createRow(0);
        HSSFCell cellHead;
        for (int i = 0; i < headers.length; i++) {
            cellHead = rowHead.createCell(i);
            cellHead.setCellValue(headers[i]);
        }
        try {
            Field[] fields;
            int i = 1;
            HSSFCell cellValue;
            List<String> list = Arrays.asList(columns);
            for (T object : source) {
                fields = object.getClass().getDeclaredFields();
                HSSFRow rowBody = sheet.createRow(i);
                // 创建序号
                cellValue = rowBody.createCell(0);
                cellValue.setCellValue(i);
                // 序号后，每列对应传入的字段columns
                for (Field field : fields) {
                    field.setAccessible(true);
                    int index = list.indexOf(field.getName());
                    if (index < 0) {
                        continue;
                    }
                    cellValue = rowBody.createCell(index + 1);
                    cellValue.setCellValue(String.valueOf(field.get(object)));
                }
                i++;
            }
            workbook.write(out);
            return out.toByteArray();
        } catch (IOException | IllegalAccessException e) {
            logger.error("导出错误");
            throw new ServiceException("export error.");
        } finally {
            try {
                out.close();
                workbook.close();
            } catch (IOException e) {
                logger.error("输出流关闭错误");
            }
        }
    }

    public static <T> byte[] export2(String sheetName, List<String> headers, List<String> columns, List<T> source) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth(15);
        HSSFRow rowHead = sheet.createRow(0);
        HSSFCell cellHead;
        for (int i = 0; i < headers.size(); i++) {
            cellHead = rowHead.createCell(i);
            cellHead.setCellValue(headers.get(i));
        }
        try {
            Field[] fields;
            int i = 1;
            HSSFCell cellValue;
            for (T object : source) {
                fields = object.getClass().getDeclaredFields();
                HSSFRow rowBody = sheet.createRow(i);
                for (Field field : fields) {
                    field.setAccessible(true);
                    int index = columns.indexOf(field.getName());
                    if (index < 0) {
                        continue;
                    }
                    cellValue = rowBody.createCell(index);
                    cellValue.setCellValue(field.get(object) != null ? String.valueOf(field.get(object)) : "");
                }
                i++;
            }
            workbook.write(out);
            return out.toByteArray();
        } catch (IOException | IllegalAccessException e) {
            logger.error("导出错误");
            throw new ServiceException("export error.");
        } finally {
            try {
                out.close();
                workbook.close();
            } catch (IOException e) {
                logger.error("输出流关闭错误");
            }
        }
    }
}
