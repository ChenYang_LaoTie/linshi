package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * LatestVideo
 */
@Data
public class LatestVideo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2405848108992640779L;
    /**
     * 主键
     */
    private Integer id;
    private String title;
    private String image;
    private String link;
    private String lang;
    private String code;
    private String supercode;
    private Integer sort;
    private List<LatestVideo> childList;
}
