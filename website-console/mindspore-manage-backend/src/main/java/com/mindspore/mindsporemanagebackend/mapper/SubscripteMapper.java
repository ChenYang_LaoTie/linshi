package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.Email;

import java.util.List;

public interface SubscripteMapper {

    List<Email> selectEmailByLang(String lang);

    List<Email> selectEmail();

    Integer countSubscription();

    Integer countCancelSubscription();

}
