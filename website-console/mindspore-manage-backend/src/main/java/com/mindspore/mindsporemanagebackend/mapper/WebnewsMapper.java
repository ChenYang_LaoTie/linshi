package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.MsNews;

public interface WebnewsMapper {
    void updateWebnews(MsNews news);
}
