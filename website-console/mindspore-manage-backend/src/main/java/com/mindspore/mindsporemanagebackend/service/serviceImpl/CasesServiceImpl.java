package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.druid.util.StringUtils;
import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;
import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.common.util.TimeUtil;
import com.mindspore.mindsporemanagebackend.mapper.CasesMapper;
import com.mindspore.mindsporemanagebackend.pojo.Cases;
import com.mindspore.mindsporemanagebackend.pojo.CasesCategory;
import com.mindspore.mindsporemanagebackend.pojo.response.CasesCategoryResp;
import com.mindspore.mindsporemanagebackend.service.CasesService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CasesServiceImpl implements CasesService {

    @Autowired
    private CasesMapper mapper;

    static final String TYPE_ERROR = "类型不匹配";
    static final Integer SEQUENCENO = 999;

    @Override
    public List<CasesCategoryResp> getCasesCategoryDropDown() {
        List<CasesCategoryResp> result = new LinkedList<CasesCategoryResp>();
        Map<Integer, LinkedList<CasesCategoryResp>> map = new LinkedHashMap<Integer, LinkedList<CasesCategoryResp>>();
        List<CasesCategory> allList = mapper.getCasesCategoryList();
        CasesCategoryResp resp = null;
        for (CasesCategory category : allList) {
            resp = new CasesCategoryResp();
            resp.setId(category.getId());
            resp.setName(category.getCatalog());
            resp.setSequenceNo(category.getSequenceNo());
            if (category.getSuperId() == null) {
                result.add(resp);
            } else {
                LinkedList<CasesCategoryResp> list = map.get(category.getSuperId());
                if (list == null) {
                    list = new LinkedList<CasesCategoryResp>();
                }
                list.add(resp);
                map.put(category.getSuperId(), list);
            }
        }


        if (result.size() > 0) {
            for (int i = 0; i < result.size(); i++) {
                LinkedList<CasesCategoryResp> innerList = map.get(result.get(i).getId());
                if (innerList == null) {
                    continue;
                }
                for (int a = 0; a < innerList.size(); a++) {
                    innerList.get(a).setChild(map.get(innerList.get(a).getId()));
                }
                result.get(i).setChild(innerList);
            }
        }
        return result;
    }

    @Override
    public void addCasesCategory(CasesCategory casesCategory) {
        if (casesCategory != null && !StringUtils.isEmpty(casesCategory.getCatalog())) {
            if (casesCategory.getSequenceNo() == null) {
                casesCategory.setSequenceNo(SEQUENCENO);
            }
            casesCategory.setUpdateTime(TimeUtil.getDate(System.currentTimeMillis()));
            mapper.saveOrUpdateCasesCategory(casesCategory);
        }
    }

    @Override
    public CasesCategory findCasesCategory(Integer id) {
        return mapper.findCasesCategoryById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String deleteCasesCategory(Integer id) {
        String result = null;
        CasesCategory category = mapper.findCasesCategoryById(id);
        List<CasesCategory> list = mapper.getCasesCategoryListBySuperId(id);
        if (list != null && list.size() > 0) {
            return "已有下级分类";
        }
        List<Cases> casesList = mapper.getCasesListByCategoryId(category.getId());
        if (casesList != null && casesList.size() > 0) {
            result = "已有案例";
        } else {
            mapper.deleteCasesCategoryByIds(category.getId());
        }
        return result;
    }

    @Override
    public FileVo upload(MultipartFile uploadFile, String type) {
        FileVo fileVo = new FileVo();

        // 1.获取文件名称,adc/jpg/JPG
        String fileName = uploadFile.getOriginalFilename();
        fileVo.setTrueName(fileName);
        if (type.equals(StringConstants.CASES_LOGO)) {
            // 3.利用正则表达式判断
            if (!fileName.matches("^.+\\.(png|jpg|PNG|JPG|jpeg|JPEG)$")) {
                // 表示文件类型不匹配
                fileVo.setTag(TYPE_ERROR);
                fileVo.setErrno(1);
                return fileVo;
            }
        }

        // 4.判断是否为恶意程序
        try {
            boolean isGif = fileName.toUpperCase().endsWith(".GIF");
            int height = 0;
            int width = 0;
            if (!isGif) {
                if (type.equals(StringConstants.CASES_LOGO)) {
                    BufferedImage image = ImageIO.read(uploadFile.getInputStream());
                    // 4.1获取宽带和高度
                    height = image.getHeight();
                    width = image.getWidth();
                    // 4.2判断属性是否为0
                    if (width == 0 || height == 0) {
                        fileVo.setErrno(1);
                        return fileVo;
                    }
                }
            }
            // 5.根据时间生成文件夹
            String localDir = String.format("%s%s/%s", PathConstants.imageDirPath, StringConstants.CASES_LOGO, type);
            File dirFile = new File(localDir);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            // 6.防止文件名重复
            // 6.1生成UUID
            String uuidName = UUID.randomUUID().toString().replaceAll("-", "");
            // 6.2获取文件类型.进行拼接
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            String realName = uuidName + fileType;
            // 6.3实现文件上传
            File realFile = new File(localDir + File.separator + realName);
            uploadFile.transferTo(realFile);
            fileVo.setHeight(height);
            fileVo.setWigth(width);
            // 设置图片虚拟访问路径
            String realurlPath = String.format("%s%s/%s/%s", PathConstants.imageAccessPath, StringConstants.CASES_LOGO, type, realName);
            fileVo.setUrl(realurlPath);
            fileVo.setSize(FileUtils.byteCountToDisplaySize(realFile.length()));

        } catch (IOException e) {
            log.error(e.getMessage());
            fileVo.setErrno(1);// 表示为恶意程序
            return fileVo;
        }
        return fileVo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addCases(Cases cases) {
        String updateTime = TimeUtil.getDate(System.currentTimeMillis());
        cases.setUpdateTime(updateTime);
        if (cases.getSequenceNo() == 0) {
            cases.setSequenceNo(SEQUENCENO);
        }
        mapper.saveCases(cases);
    }

    @Override
    public void updateCases(Cases cases) {
        String updateTime = TimeUtil.getDate(System.currentTimeMillis());
        cases.setUpdateTime(updateTime);
        if (cases.getSequenceNo() == 0) {
            cases.setSequenceNo(SEQUENCENO);
        }
        mapper.updateCases(cases);
    }

    @Override
    public PageObject<Cases> getCasesList(Integer pageCurrent, Integer limit, String name, Integer categoryId) {
        PageObject<Cases> pageObject = new PageObject<>();
        // 对参数进行校验
        if (pageCurrent == null || pageCurrent < 1) {
            throw new ServiceException("当前页码值不正确");
        }
        // 查找总记录数,并进行校验
        int rowCount = mapper.getCasesCount(name, categoryId);
        if (rowCount == 0) {
            return pageObject;
        }

        HashMap<Integer, String> categoryMap = new HashMap<>();
        List<CasesCategory> casesCategories = mapper.getCasesCategoryList();
        if (casesCategories != null && casesCategories.size() > 0) {
            for (CasesCategory cases : casesCategories) {
                categoryMap.put(cases.getId(), cases.getCatalog());
            }
        }

        Map<String, Integer> map = new HashMap<>();
        map.put("totalNum", rowCount);
        int startIndex = (pageCurrent - 1) * limit;
        List<Cases> records = mapper.getCasesList(startIndex, limit, name, categoryId);
        if (records != null && records.size() > 0) {
            for (Cases cases : records) {
                cases.setTypeName(categoryMap.get(cases.getTypeId()));
                cases.setCategoryName(categoryMap.get(cases.getCategoryId()));
            }
        }
        pageObject.setPage(pageCurrent).setTotalNum(map).setRecords(records).setSize(limit);
        return pageObject;
    }

    @Override
    public String releaseCases(Integer[] ids) {
        mapper.releaseCases(ids);
        return null;
    }

    @Override
    public String unshelfCases(Integer[] ids) {
        mapper.unshelfCases(ids);
        return null;
    }

    @Override
    public String deleteCases(Integer[] ids) {
        mapper.deleteCases(ids);
        return null;
    }

}
