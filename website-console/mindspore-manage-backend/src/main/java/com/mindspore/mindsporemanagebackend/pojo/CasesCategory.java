package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator 案例分类
 *
 */
@Data
public class CasesCategory implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8748725427425146664L;

    private Integer id;

    /**
     * 目录名称
     */
    private String catalog;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 上层目录编号
     */
    private Integer superId;

    /**
     * 目录序号
     */
    private Integer sequenceNo;

    private String updateTime;
}
