package com.mindspore.mindsporemanagebackend.controller;

import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.common.exception.ServiceException;
import com.mindspore.mindsporemanagebackend.pojo.Course;
import com.mindspore.mindsporemanagebackend.pojo.CourseCategory;
import com.mindspore.mindsporemanagebackend.pojo.CourseInstructor;
import com.mindspore.mindsporemanagebackend.pojo.CoursePopular;
import com.mindspore.mindsporemanagebackend.service.CourseService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;
import io.micrometer.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/source")
public class CourseController {

    @Autowired
    private CourseService courseService;


    @RequestMapping(path = "/course/coursepopular/list", method = RequestMethod.GET)
    public SysResult getCoursePopularList(String lang) {
        return SysResult.ok(courseService.getCoursePopularList(lang));
    }


    @RequestMapping(path = "/coursecategory/dropdown", method = RequestMethod.GET)
    public SysResult getCourseCategoryDropDown(String lang) {
        if (StringUtils.isEmpty(lang)) {
            lang = "zh";
        }

        return SysResult.ok(courseService.getCourseCategoryDropDown(lang));
    }

    @RequestMapping(path = "/coursecategory/cover/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCourseCategoryFile(MultipartFile uploadFile) {
        return courseService.upload(uploadFile, StringConstants.COURSE_CATEGORY_COVER);
    }

    @RequestMapping(path = "/coursepopular/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCoursePopularFile(MultipartFile uploadFile) {
        return courseService.upload(uploadFile, StringConstants.COURSE_POPULAR);
    }

    @RequestMapping(path = "/coursepopular/add", method = RequestMethod.POST)
    public SysResult addCoursePopular(CoursePopular coursepopular) {
        courseService.addCoursePopular(coursepopular);
        return SysResult.ok();
    }

    @RequestMapping(path = "/coursepopular/delete", method = RequestMethod.GET)
    public SysResult deleteCoursePopular(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要删除项", null);
        }
        String result = courseService.deleteCoursePopular(id);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("热门推荐删除失败", result);
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/coursecategory/add", method = RequestMethod.POST)
    public SysResult insertCourseCategory(CourseCategory courseCategory) {
        if (StringUtils.isEmpty(courseCategory.getLang())) {
            courseCategory.setLang("zh");
        }
        if (!StringUtils.isEmpty(courseCategory.getCourseDescription())) {
            if (courseCategory.getCourseDescription().contains("&amp;")) {
                courseCategory.setCourseDescription(courseCategory.getCourseDescription().replace("amp;", ""));
            }
        }
        courseService.addCourseCategory(courseCategory);
        return SysResult.ok();
    }

    @RequestMapping(path = "/coursecategory/info", method = RequestMethod.GET)
    public SysResult findCourseCategory(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要查询的课程目录", null);
        }
        CourseCategory result = courseService.findCourseCategory(id);
        return SysResult.ok(result);
    }

    @RequestMapping(path = "/coursecategory/delete", method = RequestMethod.GET)
    public SysResult deleteCourseCategory(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要删除的课程目录", null);
        }
        String result = courseService.deleteCourseCategory(id);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("课程目录删除失败", result);
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/courseinstructor/avatar/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCourseInstructorFile(MultipartFile uploadFile) {
        return courseService.upload(uploadFile, StringConstants.COURSE_INSTRUCTOR_AVATAR);
    }

    @RequestMapping(path = "/courseinstructor/add", method = RequestMethod.POST)
    public SysResult insertCourseInstructor(CourseInstructor instructor) {
        courseService.addCourseInstructor(instructor);
        return SysResult.ok();
    }

    @RequestMapping(path = "/courseinstructor/delete", method = RequestMethod.GET)
    public SysResult deleteCourseInstructor(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要删除的讲师", null);
        }
        String result = courseService.deleteCourseInstructor(id);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("讲师删除失败", result);
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/courseinstructor/search", method = RequestMethod.GET)
    public SysResult searchCourseInstructor(String name) {
        return SysResult.ok(courseService.searchCourseInstructor(name));
    }

    @RequestMapping(path = "/courseinstructor/list", method = RequestMethod.GET)
    public PageObject<CourseInstructor> getCourseInstructor(Integer pageCurrent, Integer limit, String name) {
        return courseService.getCourseInstructorList(pageCurrent, limit, name);
    }

    @RequestMapping(path = "/course/video/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCourseVideoFile(MultipartFile uploadFile) {
        return courseService.upload(uploadFile, StringConstants.COURSE_VIDEO);
    }

    @RequestMapping(path = "/course/document/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCourseDocumentFile(MultipartFile uploadFile) {
        return courseService.upload(uploadFile, StringConstants.COURSE_DOCUMENT);
    }

    @RequestMapping(path = "/course/image/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCourseImageFile(MultipartFile uploadFile) {
        return courseService.upload(uploadFile, StringConstants.COURSE_IMAGE);
    }

    @RequestMapping(path = "/course/add", method = RequestMethod.POST)
    public SysResult insertCourse(Course course) {
        if (StringUtils.isEmpty(course.getLang())) {
            course.setLang("zh");
        }
        if (course.getTitle().contains("&amp;")) {
            course.setTitle(course.getTitle().replace("amp;", ""));
        }
        courseService.addCourse(course);
        return SysResult.ok();
    }

    @RequestMapping(path = "/course/update", method = RequestMethod.POST)
    public SysResult updateCourse(Course course) {
        if (course.getId() == null) {
            return SysResult.fail("id is null", null);
        }
        if (StringUtils.isEmpty(course.getLang())) {
            course.setLang("zh");
        }
        if (course.getTitle().contains("&amp;")) {
            course.setTitle(course.getTitle().replace("amp;", ""));
        }
        courseService.updateCourse(course);
        return SysResult.ok();
    }

    @RequestMapping(path = "/course/info", method = RequestMethod.GET)
    public SysResult getCourseInfo(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择课程", null);
        }
        Course result = courseService.getCourseInfo(id);
        return SysResult.ok(result);
    }

    @RequestMapping(path = "/course/list", method = RequestMethod.GET)
    public PageObject<Course> getCourseList(Integer pageCurrent, Integer limit, String name, Integer categoryId, String lang) {
        // 对参数进行校验
        if (pageCurrent == null || pageCurrent < 1) {
            throw new ServiceException("当前页码值不正确");
        }
        if (StringUtils.isEmpty(lang)) {
            lang = "zh";
        }
        return courseService.getCourseList(pageCurrent, limit, name, categoryId, lang);
    }

    @RequestMapping(path = "/course/release", method = RequestMethod.GET)
    public SysResult releaseCourse(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要上架的课程", null);
        }
        String result = courseService.releaseCourse(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("课程上架失败", "");
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/course/unshelf", method = RequestMethod.GET)
    public SysResult unshelfCourse(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要下架的课程", null);
        }
        String result = courseService.unshelfCourse(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("课程下架失败", "");
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/course/delete", method = RequestMethod.GET)
    public SysResult deleteCourse(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要删除的课程", null);
        }
        String result = courseService.deleteCourse(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("课程删除失败", "");
        }
        return SysResult.ok();
    }


}
