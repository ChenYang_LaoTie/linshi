package com.mindspore.mindsporemanagebackend.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.mindspore.mindsporemanagebackend.security.vo.AccessToken;
import com.mindspore.mindsporemanagebackend.security.vo.UserInfo;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class TenantFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        try {
            Cookie accessToken = verifyCookie(request, OIDCConfig.ACCESS_TOKEN);
            Cookie refreshToken = verifyCookie(request, OIDCConfig.REFRESH_TOKEN);
    
            if (accessToken == null || refreshToken == null) {
                throw new Exception("can not get access_token or refresh_token");
            }
            
            UserInfo userInfo = preHandle(accessToken);

            SecurityContext context = SecurityContextHolder.createEmptyContext(); 

            List<GrantedAuthority> authorities = new ArrayList<>(); 
            authorities.add((GrantedAuthority) () -> "USER");
            UsernamePasswordAuthenticationToken authenticateAction = new UsernamePasswordAuthenticationToken(userInfo, accessToken.getValue(), authorities);

            context.setAuthentication(authenticateAction);
            SecurityContextHolder.setContext(context);

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        filterChain.doFilter(request, response);
    }

    public UserInfo preHandle(Cookie accessToken) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", accessToken.getValue());
        HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<UserInfo> responseEntity =  restTemplate.exchange(OIDCConfig.USER_INFO_URL, HttpMethod.GET, httpEntity, UserInfo.class);
        return responseEntity.getBody();
    }

    private Cookie verifyCookie(HttpServletRequest httpServletRequest, String cookieName) {
        Cookie[] cookies = httpServletRequest.getCookies();
        Cookie cookie = null;
        if (cookies != null) {
            Optional<Cookie> first = Arrays.stream(cookies).filter(c -> cookieName.equals(c.getName())).findFirst();
            if (first.isPresent()) {
                cookie = first.get();
            }
        }
        return cookie;
    }




}
