package com.mindspore.mindsporemanagebackend.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.mindspore.mindsporemanagebackend.pojo.Dissertation;
import com.mindspore.mindsporemanagebackend.pojo.DissertationCategory;
import com.mindspore.mindsporemanagebackend.pojo.DissertationSearch;

public interface DissertationMapper {

    int getRowCount(DissertationSearch search);

    List<Dissertation> getDissertationList(DissertationSearch search);

    @Select("SELECT * FROM dissertationcategory WHERE type in('domain','source') ORDER BY sequence,id")
    List<DissertationCategory> getDissertationCategory();

    void insertDissertation(@Param("diss") Dissertation diss);

    void updateDissertation(@Param("diss") Dissertation diss);

    void updateDissertationStatus(@Param("status") int status, @Param("id") Integer id);

    void deleteDissertation(@Param("ids") Integer... ids);

    void insertDissertationCategory(@Param("diss") DissertationCategory diss);

    void updateDissertationCategory(@Param("diss") DissertationCategory diss);

    @Select("SELECT * FROM dissertationcategory WHERE id = #{id}")
    DissertationCategory getDissertationCategoryById(Integer id);

    @Select("SELECT * FROM dissertation where sources = #{id} or domain = #{id}")
    List<Dissertation> getDissertationListByDomainOrSources(Integer id);

    @Delete("DELETE FROM dissertationcategory WHERE id = #{id}")
    void deleteDissertationCategory(Integer id);

    @Select("SELECT * FROM dissertationcategory WHERE lang=#{lang} and type=#{type} ORDER BY sequence,id")
    List<DissertationCategory> getDissertationCategoryList(@Param("lang") String lang, @Param("type") String type);

}
