package com.mindspore.mindsporemanagebackend.security;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.mindspore.mindsporemanagebackend.security.vo.AccessToken;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
@CrossOrigin("*")
public class SetTokenController {

    @GetMapping("/token")
    @ResponseBody
    public void getTokenByCode(HttpServletRequest request, HttpServletResponse response, String code) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("code", code);
        map.add("client_id", OIDCConfig.CLIENT_ID);
        map.add("client_secret", OIDCConfig.CLIENT_SECRET);
        map.add("grant_type", "authorization_code");
        map.add("redirect_uri", OIDCConfig.REDIRECT_URL);

        HttpEntity<MultiValueMap<String, String>> tokenRequest = new HttpEntity<>(map, headers);

        ResponseEntity<AccessToken> tokenResponse = restTemplate.postForEntity(OIDCConfig.TOKEN_URL, tokenRequest , AccessToken.class);
        
        AccessToken accessToken = tokenResponse.getBody();
        if (null == accessToken) {
            response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
            return;
        }
        
        Cookie accessTokenCookie = new Cookie(OIDCConfig.ACCESS_TOKEN, accessToken.getAccessToken());
        accessTokenCookie.setPath("/");
    
        Cookie refreshTokenCookie = new Cookie(OIDCConfig.REFRESH_TOKEN, accessToken.getRefreshToken());
        refreshTokenCookie.setPath("/");

        response.addCookie(accessTokenCookie);
        response.addCookie(refreshTokenCookie);
    }
}
