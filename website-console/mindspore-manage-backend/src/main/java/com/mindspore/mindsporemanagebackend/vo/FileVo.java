package com.mindspore.mindsporemanagebackend.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class FileVo {

    private Integer errno = 0;
    private String url;// 表示图片保存路径
    private Integer wigth;
    private Integer height;
    private String tag;
    private String trueName;
    private String size;

    public FileVo() {

    }

    public FileVo(Integer errno, String url, Integer wigth, Integer height) {
        super();
        this.errno = errno;
        this.url = url;
        this.wigth = wigth;
        this.height = height;
    }
}
