package com.mindspore.mindsporemanagebackend.common.constant;

public class PathConstants {
    public static final String imageDirPath = "/mnt/share/file/";
    public static final String imageAccessPath = "/file/";
}
