package com.mindspore.mindsporemanagebackend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.mindspore.mindsporemanagebackend.mapper")
public class MindsporeManageBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MindsporeManageBackendApplication.class, args);
    }

}
