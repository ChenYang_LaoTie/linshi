package com.mindspore.mindsporemanagebackend.pojo;

import java.util.List;

/**
 * @author haha
 * @since 2020-07-23
 * @version 1
 */
public class ClickStatistic {
    private String name;
    private Integer clicks;
    private Integer clicksByIp;
    private List<ClickFromPage> clickFromPages;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getClicksByIp() {
        return clicksByIp;
    }

    public void setClicksByIp(Integer clicksByIp) {
        this.clicksByIp = clicksByIp;
    }

    public List<ClickFromPage> getClickFromPages() {
        return clickFromPages;
    }

    public void setClickFromPages(List<ClickFromPage> clickFromPages) {
        this.clickFromPages = clickFromPages;
    }
}
