package com.mindspore.mindsporemanagebackend.pojo.entity;

import lombok.Data;

/**
 * 竞赛活动
 *
 * @author yuanyan
 * @version 0.1
 * @create 2021/12/9
 */
@Data
public class Competition {
    private Integer id;

    // 语言
    private String lang;

    // 竞赛标题
    private String title;

    // 竞赛介绍
    private String introduce;

    // 竞赛状态 CompetitionStatus
    private String competitionStatus;

    // 发布状态 PublishStatus
    private String publishStatus;

    // 奖金
    private String prize;

    // 举办方
    private String organizer;

    // 开始日期
    private String startDay;

    // 截止日期
    private String endDay;

    // 初赛截止日期
    private String preliminaryEndDay;

    // 竞赛链接
    private String link;

    private String createTime;

    private String updateTime;
}
