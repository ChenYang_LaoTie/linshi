package com.mindspore.mindsporemanagebackend.pojo;

import com.mindspore.mindsporemanagebackend.common.constant.NumberConstants;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 用于评价导出
 */
@Accessors(chain = true)
@Data
public class ExportEssay {

    private Integer oneGrade = 0;
    private Integer twoGrade = 0;
    private Integer threeGrade = 0;
    private Integer fourGrade = 0;
    private Integer fiveGrade = 0;
    private String branch;
    private String language;
    private String title;
    private String parent;
    private String twoParent;
    
    public ExportEssay(@NotNull Essay essay) {
        Optional.ofNullable(essay.getTitle()).ifPresent(title -> {
            Pattern pattern = Pattern.compile("^.*\\/(.*)\\.html$");
            Matcher matcher = pattern.matcher(title);
            if (matcher.find()) {
                this.title = matcher.group(NumberConstants.ONE);
            } else {
                this.title = title;
            }
        });
        this.parent = essay.getParent();
        this.addEssay(essay);
    }

    public ExportEssay addEssay(Essay essay) {
        switch (essay.getScore()) {
            case 1:
                this.oneGrade++;
                break;
            case 2:
                this.twoGrade++;
                break;
            case 3:
                this.threeGrade++;
                break;
            case 4:
                this.fourGrade++;
                break;
            case 5:
                this.fiveGrade++;
                break;
        }
        return this;
    }
}
