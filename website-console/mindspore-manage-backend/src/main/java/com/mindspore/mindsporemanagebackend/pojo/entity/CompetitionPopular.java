package com.mindspore.mindsporemanagebackend.pojo.entity;

import lombok.Data;

/**
 * 热门竞赛推荐
 *
 * @author yuanyan
 * @version 0.1
 * @create 2021/12/9
 */
@Data
public class CompetitionPopular {
    private Integer id;

    // 语言
    private String lang;

    // 名称
    private String name;

    // 连接
    private String link;

    // 排序
    private Integer sequence;

    // 图片
    private String image;

    private String createTime;

    private String updateTime;
}
