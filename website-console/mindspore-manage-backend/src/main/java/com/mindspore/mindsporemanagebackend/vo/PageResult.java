package com.mindspore.mindsporemanagebackend.vo;

import java.util.List;

/**
 * @author
 * @since 2020-7-16 14:22:35
 * @version 1
 */
public class PageResult<T> {
    private Integer current;
    private Integer limit;
    private Integer total;
    private List<T> data;

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
