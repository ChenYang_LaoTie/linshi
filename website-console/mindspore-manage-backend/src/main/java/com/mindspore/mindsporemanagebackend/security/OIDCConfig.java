package com.mindspore.mindsporemanagebackend.security;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class OIDCConfig implements InitializingBean {


    @Value("${oidc.client-id}")
    private String clientId;
    
    @Value("${oidc.client-secret}")
    private String clientSecret;

    @Value("${oidc.redirect-url}")
    private String redirectUrl; 

    @Value("${oidc.authorization-url}")
    private String authorizationUrl;

    @Value("${oidc.token-url}")
    private String tokenUrl;

    @Value("${oidc.user-info-url}")
    private String userInfoUrl;

    @Value("${oidc.jwk-set-url}")
    private String jwkSetUrl;



    public static String CLIENT_ID;
    public static String CLIENT_SECRET;
    public static String REDIRECT_URL;
    public static String AUTHORIZATION_URL;
    public static String TOKEN_URL;
    public static String USER_INFO_URL;
    public static String JWK_SET_URL;

    public static String ACCESS_TOKEN;
    public static String REFRESH_TOKEN;

    
    @Override
    public void afterPropertiesSet() throws Exception {
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        REDIRECT_URL = redirectUrl;
        AUTHORIZATION_URL = authorizationUrl;
        TOKEN_URL = tokenUrl;
        USER_INFO_URL = userInfoUrl;
        JWK_SET_URL = jwkSetUrl;

        ACCESS_TOKEN = "access_token";
        REFRESH_TOKEN = "refresh_token";
    }

}
