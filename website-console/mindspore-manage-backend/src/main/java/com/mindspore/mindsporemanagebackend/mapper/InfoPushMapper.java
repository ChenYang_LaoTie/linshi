package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.Information;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InfoPushMapper {

    void saveInfo(Information info);

    void deleteInfoBynewsId(Integer newsId);

    int getRowCount();

    List<Information> selectInfo(@Param("startIndex") int startIndex, @Param("pageSize") int pageSize);

    List<Information> selectInfoById(@Param("ids") Integer... ids);

    void deleteInfo(@Param("ids") Integer... ids);

    void updateInfo(Information info);

    void updateInfoAfterSend(Information info);
}
