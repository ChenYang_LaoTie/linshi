package com.mindspore.mindsporemanagebackend.common.constant;

public class NumberConstants {
    public static final Integer ZERO = 0;
    public static final Integer ONE = 1;
    public static final Integer SIXTY = 60;
    public static final Integer HUNDRED = 100;
}
