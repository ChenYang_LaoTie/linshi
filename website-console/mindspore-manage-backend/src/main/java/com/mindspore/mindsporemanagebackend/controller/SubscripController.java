package com.mindspore.mindsporemanagebackend.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mindspore.mindsporemanagebackend.common.util.ExcelUtil;
import com.mindspore.mindsporemanagebackend.service.SubscripeService;
import com.mindspore.mindsporemanagebackend.vo.SysResult;

import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/source")
public class SubscripController {
    @Autowired
    private SubscripeService subscripeService;

    @RequestMapping(path = "/subscription/excel", method = RequestMethod.GET)
    public byte[] downloadSubscribeCounts(HttpServletResponse response) {
        List<Integer> list = new ArrayList<>();
        Integer countSubscription = subscripeService.countSubscription();
        list.add(countSubscription);
        Integer countCancelSubscription = subscripeService.countCancelSubscription();
        list.add(countCancelSubscription);
        String[] headers = {"订阅人数", "取消订阅人数"};
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=subscription.xls");
        return ExcelUtil.export("订阅人数表", headers, list);
    }

    @RequestMapping(path = "/subscription/metric", method = RequestMethod.GET)
    public Object metricSubscriptions() {
        Map<String, Integer> subscribeCounts = new HashMap<>(4);
        Integer countSubscription = subscripeService.countSubscription();
        subscribeCounts.put("sub", countSubscription);
        Integer countCancelSubscription = subscripeService.countCancelSubscription();
        subscribeCounts.put("cancelSub", countCancelSubscription);
        return SysResult.ok(subscribeCounts);
    }
}
