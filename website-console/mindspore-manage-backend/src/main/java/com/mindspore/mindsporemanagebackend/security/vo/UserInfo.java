package com.mindspore.mindsporemanagebackend.security.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserInfo {

    public String msg;

    public String sub;

    public String website;

    public String zoneinfo;

    public int code;

    public String birthdate;

    @JsonProperty("email_verified")
    public boolean emailVerified;



    public String gender;

    public String profile;

    @JsonProperty("phone_number_verified")
    public boolean phoneNumberVerified;

    @JsonProperty("preferred_username")
    public String preferredUsername;

    @JsonProperty("given_name")
    public String givenName;

    @JsonProperty("middle_name")
    public String middleName;

    public String locale;

    public String picture;

    @JsonProperty("updated_at")
    public String updatedAt;

    @JsonProperty("offline_access")
    public String offlineAccess;

    public String name;

    public String nickname;

    @JsonProperty("phone_number")
    public String phoneNumber;

    @JsonProperty("family_name")
    public String familyName;

    public String email;

    public String username;
}