package com.mindspore.mindsporemanagebackend.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.pojo.LatestVideo;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

public interface LatestVideoService {

    FileVo upload(MultipartFile uploadFile);

    PageObject<LatestVideo> selectLatestVideo(Integer pageCurrent, Integer limit);

    LatestVideo getLatestVideoById(Integer id);

    void insertLatestVideo(LatestVideo video);

    void updateLatestVideoById(LatestVideo video);

    void deleteLatestVideo(Integer... ids);
    
    List<LatestVideo> getLatestvideoList(String lang);

    List<LatestVideo> getLatestvideoOneLevelList(String lang);

}
