package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author
 * @version 2
 */
@Accessors(chain = true)
@Data
public class MsNews implements Serializable {
    private static final long serialVersionUID = 8406180414584241396L;
    /** 新闻id */
    private Integer id;
    /** 新闻标题 */
    private String newsTitle;
    /** 新闻描述 */
    private String newsContent;
    /** 新闻图片 */
    private String newsImage;
    /** 状态码 0表示未发布.1表示已经发布 */
    private Integer status = 0;
    /** 区分中英文 */
    private String tag;
    private String creatTime;
    private String newsTime;
    /**
     * 0-全部 1 - 版本发布 2 - 技术博客 3 - 活动公告 4 - 每日新闻 5- 案例
     */
    private Integer type;
    /**
     * 技术博客分类
     */
    private String category;
    /** 新闻详情信息 */
    private String newsDetail;
}
