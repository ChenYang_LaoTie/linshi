package com.mindspore.mindsporemanagebackend.service.serviceImpl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindspore.mindsporemanagebackend.mapper.ClickVideoMapper;
import com.mindspore.mindsporemanagebackend.pojo.ClickFromPage;
import com.mindspore.mindsporemanagebackend.pojo.ClickStatistic;
import com.mindspore.mindsporemanagebackend.pojo.ClickVideo;
import com.mindspore.mindsporemanagebackend.service.ClickVideoService;
import com.mindspore.mindsporemanagebackend.vo.PageResult;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ClickVideoServiceImpl implements ClickVideoService {
    @Autowired
    private ClickVideoMapper clickVideoMapper;

    @Override
    public PageResult<ClickStatistic> selectStatisticByPage(Integer current, Integer limit) {
        PageResult<ClickStatistic> page = new PageResult<>();
        page.setTotal(clickVideoMapper.countSelectAllName());
        page.setLimit(limit);
        page.setCurrent(current);

        List<ClickVideo> sourceByNameByPage = clickVideoMapper.selectAllNameByPage((current - 1) * limit, limit);
        List<ClickStatistic> result = new ArrayList<>();

        sourceByNameByPage.forEach(clickVideo -> {
            ClickStatistic statistic = new ClickStatistic();
            statistic.setName(clickVideo.getName());
            statistic.setClicks(clickVideo.getCount());

            List<ClickFromPage> clickFromPages = new ArrayList<>();
            List<ClickVideo> sourceByNameAndFrom = clickVideoMapper.selectFromCountsByNameOnly(clickVideo.getName());

            sourceByNameAndFrom.forEach(clickVideo1 -> {
                ClickFromPage clickFromPage = new ClickFromPage();
                clickFromPage.setFrom(clickVideo1.getFrom());
                clickFromPage.setFromCounts(clickVideo1.getCount());
                clickFromPages.add(clickFromPage);

            });
            statistic.setClickFromPages(clickFromPages);

            int ipCounts = clickVideoMapper.selectIpCountsByNameOnly(clickVideo.getName());
            statistic.setClicksByIp(ipCounts);
            result.add(statistic);
        });
        page.setData(result);
        return page;
    }

    @Override
    public List<ClickStatistic> statistic(String beginTime, String endTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String begin = dateTimeFormatter.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(beginTime)), ZoneId.systemDefault()));
        String end = dateTimeFormatter.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(endTime)), ZoneId.systemDefault()));

        // step 1: get all video name by time
        List<ClickVideo> sourceByName = clickVideoMapper.selectAllNameByTime(begin, end);
        List<ClickStatistic> result = new ArrayList<>();
        sourceByName.forEach(clickVideo -> {
            ClickStatistic statistic = new ClickStatistic();
            statistic.setName(clickVideo.getName());
            statistic.setClicks(clickVideo.getCount());

            // step2: get count from which page by video name;
            List<ClickFromPage> clickFromPages = new ArrayList<>();
            List<ClickVideo> sourceByNameAndFrom =
                    clickVideoMapper.selectFromCountsByName(begin, end, clickVideo.getName());
            sourceByNameAndFrom.forEach(clickVideo1 -> {
                ClickFromPage clickFromPage = new ClickFromPage();
                clickFromPage.setFrom(clickVideo1.getFrom());
                clickFromPage.setFromCounts(clickVideo1.getCount());
                clickFromPages.add(clickFromPage);
            });
            statistic.setClickFromPages(clickFromPages);

            // step3: ip counts
            int ipCounts = clickVideoMapper.selectIpCountsByName(begin, end, clickVideo.getName());
            statistic.setClicksByIp(ipCounts);
            result.add(statistic);
        });
        return result;
    }
}