package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.Modeled;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ModeledMapper {

    void addOrUpdateModeled(Modeled modeled);

    @Select("select count(id) from modeled")
    int getModeledCount();

    @Select("select * from modeled order by id desc limit #{startIndex},#{limit}")
    List<Modeled> getModeledList(@Param("startIndex") int startIndex, @Param("limit") Integer limit);

    void releaseModeled(@Param("ids") Integer... ids);

    void unshelfModeled(@Param("ids") Integer... ids);

    void deleteModeledByIds(@Param("ids") Integer... ids);
}
