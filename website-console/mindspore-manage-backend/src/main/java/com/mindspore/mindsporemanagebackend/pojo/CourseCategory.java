package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator 课程分类
 *
 */
@Data
public class CourseCategory implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4018698578102997082L;
    private Integer id;

    /**
     * 目录名称
     */
    private String catalog;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 语言
     */
    private String lang;

    /**
     * 上层目录编号
     */
    private Integer superId;

    /**
     * 目录序号
     */
    private Integer sequenceNo;
    /**
     * 目录描述
     */
    private String description;

    /**
     * 目录封面
     */
    private String coverImg;

    /**
     * 系列课程团队编号 3级目录上增加
     */
    private String teamId;
    
    /**
     * 系列课程简介 3级目录上增加
     */
    private String courseDescription;

    private String updateTime;
}
