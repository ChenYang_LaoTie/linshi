package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Essay2Forum;
import com.mindspore.mindsporemanagebackend.pojo.ExportEssay;
import com.mindspore.mindsporemanagebackend.pojo.entity.EssayJump;
import com.mindspore.mindsporemanagebackend.pojo.response.EssayScoreSummary;
import com.mindspore.mindsporemanagebackend.vo.PageResult;

import java.util.Collection;
import java.util.List;

public interface EssayService {

    Collection<ExportEssay> exportEssay(String start, String end);

    PageResult<Essay2Forum> selectByPage(int page, int limit);

    List<Essay2Forum> selectAll();

    PageResult<EssayScoreSummary> queryEssayScoreSummaries(Integer pageCurrent, Integer limit, String startTime, String endTime);

    List<EssayScoreSummary> getEssayScoreSummaries(Integer pageCurrent, Integer limit, String startTime, String endTime);

    PageResult<EssayJump> queryEssayJumps(Integer pageCurrent, Integer limit);

    List<EssayJump> getEssayJumps(Integer pageCurrent, Integer limit);

}
