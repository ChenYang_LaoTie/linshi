package com.mindspore.mindsporemanagebackend.security.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AccessToken {

    @JsonProperty("access_token")
    public String accessToken;
    @JsonProperty("refresh_token")
    public String refreshToken;
    @JsonProperty("scope")
    public String scope;
    @JsonProperty("token_type")
    public String tokenType;
    @JsonProperty("expires_in")
    public String expiresIn;

    
}
