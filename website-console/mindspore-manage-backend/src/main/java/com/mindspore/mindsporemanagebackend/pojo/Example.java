package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 示例后台管理
 * 
 * @author Administrator
 *
 */
@Data
public class Example implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1519487474260116910L;

    private Integer id;
    /**
     * 示例标题
     */
    private String title;
    /**
     * 示例icon
     */
    private String icon;
    /**
     * 介绍
     */
    private String introduce;
    /**
     * Android url
     */
    private String androidUrl;
    /**
     * IOS url
     */
    private String iosUrl;
    /**
     * 发布日期
     */
    private String releaseDate;
    /**
     * 中英文
     */
    private String lang;
    /**
     * 首页展示 0不展示 1展示
     */
    private Integer showHomePage;
    /**
     * 序号
     */
    private Integer sequenceNo;
    /**
     * 状态 0 未上架 1已上架
     */
    private String status;

    private String updateTime;
    /**
     * loT设备上试试
     */
    private String lotUrl;
}
