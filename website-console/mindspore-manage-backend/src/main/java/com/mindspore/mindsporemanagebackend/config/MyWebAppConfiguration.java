package com.mindspore.mindsporemanagebackend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.mindspore.mindsporemanagebackend.common.constant.PathConstants;

@Configuration
public class MyWebAppConfiguration extends WebMvcConfigurationSupport {
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler(PathConstants.imageAccessPath + "**").addResourceLocations("file:" + PathConstants.imageDirPath);
        super.addResourceHandlers(registry);
    }
}
