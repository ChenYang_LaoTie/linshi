package com.mindspore.mindsporemanagebackend.pojo;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Resume {
    private Integer id;
    @NotNull
    @Size(max = 25)
    private String name;
    @NotNull
    @Size(max = 11)
    private String phone;
    @NotNull
    @Email
    private String email;
    private String company;
    private String job;
    private String jobExperience;
    private String technicalExperties;
    private String trainningExperience;
    private String resumePath;
    private String postTime;
    @NotNull
    @Size(max = 25)
    private String postType;
}
