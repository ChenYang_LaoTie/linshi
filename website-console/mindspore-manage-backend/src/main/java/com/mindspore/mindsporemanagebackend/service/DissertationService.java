package com.mindspore.mindsporemanagebackend.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.pojo.Dissertation;
import com.mindspore.mindsporemanagebackend.pojo.DissertationCategory;
import com.mindspore.mindsporemanagebackend.pojo.DissertationSearch;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;

public interface DissertationService {

    PageObject<Dissertation> getDissertationList(DissertationSearch search);

    HashMap<Integer, String> getCategoryMap();

    void insertDissertation(Dissertation dissertation);

    void updateDissertation(Dissertation dissertation);

    void releaseOrUnReleaseDissertation(boolean isRelease, Integer id);

    void deleteDissertation(Integer... ids);

    FileVo uploadImageFile(MultipartFile uploadFile, String popularRecommendationCover);

    void insertDissertationCategory(DissertationCategory dissertation);

    void updateDissertationCategory(DissertationCategory dissertation);

    String deleteDissertationCategory(Integer id);

    HashMap<String, List<DissertationCategory>> getDissertationCategoryList(String lang);

}
