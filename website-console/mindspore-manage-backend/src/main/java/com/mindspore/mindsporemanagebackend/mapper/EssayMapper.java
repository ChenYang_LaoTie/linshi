package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.Essay;
import com.mindspore.mindsporemanagebackend.pojo.Essay2Forum;
import com.mindspore.mindsporemanagebackend.pojo.entity.EssayJump;
import com.mindspore.mindsporemanagebackend.pojo.response.EssayScoreSummary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EssayMapper {

    List<Essay> queryEssays(String start, String end);

    int getTotal();

    List<Essay2Forum> selectByPage(@Param("begin") int begin, @Param("limit") int limit);

    List<Essay2Forum> selectAll();

    int getEssayScoreSummaryCount(@Param("startTime") String startTime, @Param("endTime") String endTime);

    List<EssayScoreSummary> queryEssayScoreSummaries(@Param("offset") Integer offset, @Param("limit") Integer limit, @Param("startTime") String startTime, @Param("endTime") String endTime);

    int getEssayJumpCount();

    List<EssayJump> queryEssayJumps(@Param("offset") Integer offset, @Param("limit") Integer limit);
}
