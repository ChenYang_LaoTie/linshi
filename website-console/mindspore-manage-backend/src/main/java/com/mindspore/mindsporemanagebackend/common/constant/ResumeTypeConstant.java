package com.mindspore.mindsporemanagebackend.common.constant;

import java.util.HashMap;
import java.util.Map;

public final class ResumeTypeConstant {
    public static final Map<String, String> RESUME_TYPE = new HashMap<>();
    static {
        RESUME_TYPE.put("developer", "优秀开发者");
        RESUME_TYPE.put("senior_developer", "资深开发者");
        RESUME_TYPE.put("evangelist", "布道师");
        RESUME_TYPE.put("senior_evangelist", "资深布道师");
    }
}
