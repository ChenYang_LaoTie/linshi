package com.mindspore.mindsporemanagebackend.pojo.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CasesCategoryResp {

    private Integer id;

    private String name;
    
    private Integer sequenceNo;
    
    private List<CasesCategoryResp> child;
}
