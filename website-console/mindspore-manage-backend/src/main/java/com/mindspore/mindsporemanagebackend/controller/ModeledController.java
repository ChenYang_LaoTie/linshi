package com.mindspore.mindsporemanagebackend.controller;

import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.pojo.Modeled;
import com.mindspore.mindsporemanagebackend.service.ModeledService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;
import io.micrometer.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/source")
public class ModeledController {

    @Autowired
    private ModeledService modeledService;

    @RequestMapping(path = "/modeled/icon/uploadFile", method = RequestMethod.POST)
    public FileVo uploadImageFile(MultipartFile uploadFile) {
        return modeledService.upload(uploadFile, StringConstants.MODELED_ICON);
    }

    @RequestMapping(path = "/modeled/add", method = RequestMethod.POST)
    public SysResult addOrUpdateModeled(Modeled modeled) {
        modeledService.addOrUpdateModeled(modeled);
        return SysResult.ok();
    }

    @RequestMapping(path = "/modeled/list", method = RequestMethod.GET)
    public PageObject<Modeled> getModeledList(Integer pageCurrent, Integer limit) {
        return modeledService.getModeledList(pageCurrent, limit);
    }

    @RequestMapping(path = "/modeled/release", method = RequestMethod.GET)
    public SysResult releaseModeled(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要上架的示例", null);
        }
        String result = modeledService.releaseModeled(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("示例上架失败", "");
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/modeled/unshelf", method = RequestMethod.GET)
    public SysResult unshelfModeled(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要下架的示例", null);
        }
        String result = modeledService.unshelfModeled(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("示例下架失败", "");
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/modeled/delete", method = RequestMethod.GET)
    public SysResult deleteModeled(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要删除的示例", null);
        }
        String result = modeledService.deleteModeled(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("示例删除失败", "");
        }
        return SysResult.ok();
    }

}
