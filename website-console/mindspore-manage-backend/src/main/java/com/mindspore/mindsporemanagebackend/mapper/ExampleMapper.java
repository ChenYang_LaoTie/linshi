package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.Example;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ExampleMapper {

    void addOrUpdateExample(Example example);

    @Select("select count(id) from example")
    int getExampleCount();

    @Select("select * from example order by id desc limit #{startIndex},#{limit}")
    List<Example> getExampleList(@Param("startIndex") int startIndex, @Param("limit") Integer limit);

    void releaseExample(@Param("ids") Integer... ids);

    void unshelfExample(@Param("ids") Integer... ids);

    void deleteExampleByIds(@Param("ids") Integer... ids);

}
