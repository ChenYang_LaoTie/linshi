package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Course;
import com.mindspore.mindsporemanagebackend.pojo.CourseCategory;
import com.mindspore.mindsporemanagebackend.pojo.CourseInstructor;
import com.mindspore.mindsporemanagebackend.pojo.CoursePopular;
import com.mindspore.mindsporemanagebackend.pojo.response.CourseCategoryResp;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CourseService {

    List<CoursePopular> getCoursePopularList(String lang);

    List<CourseCategoryResp> getCourseCategoryDropDown(String lang);

    FileVo upload(MultipartFile uploadFile, String type);

    void addCoursePopular(CoursePopular coursepopular);

    String deleteCoursePopular(Integer id);

    void addCourseCategory(CourseCategory courseCategory);

    CourseCategory findCourseCategory(Integer id);

    String deleteCourseCategory(Integer ids);

    void addCourseInstructor(CourseInstructor instructor);

    String deleteCourseInstructor(Integer id);

    List<CourseInstructor> searchCourseInstructor(String name);

    PageObject<CourseInstructor> getCourseInstructorList(Integer pageCurrent, Integer limit, String name);

    void addCourse(Course course);

    void updateCourse(Course course);

    Course getCourseInfo(Integer id);

    PageObject<Course> getCourseList(Integer pageCurrent, Integer limit, String name, Integer categoryId, String lang);

    String releaseCourse(Integer[] ids);

    String unshelfCourse(Integer[] ids);

    String deleteCourse(Integer[] ids);

}
