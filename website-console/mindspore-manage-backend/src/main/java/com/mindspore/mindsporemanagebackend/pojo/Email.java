package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Email implements Serializable {

    private static final long serialVersionUID = 6919040487196249802L;
    private Integer id;
    private String mail;
    private int status=1;
    private String lang = "en";

}
