package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator 案例
 *
 */
@Data
public class Cases implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8963439404440457853L;

    private Integer id;

    /**
     * 案例类型id
     */
    private Integer typeId;

    /**
     * 案例种类id
     */
    private Integer categoryId;

    /**
     * 标题
     */
    private String title;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 关键技术
     */
    private String technology;

    /**
     * 状态 0 未上架 1已上架
     */
    private String status;

    /**
     * 上架日期
     */
    private String releaseDate;

    /**
     * logo
     */
    private String logoImg;
    /**
     * 案例链接
     */
    private String casesLink;
    /**
     * 序号
     */
    private Integer sequenceNo;

    private String updateTime;

    /**
     * 案例类型
     */
    private String typeName;

    /**
     * 案例种类
     */
    private String categoryName;
}