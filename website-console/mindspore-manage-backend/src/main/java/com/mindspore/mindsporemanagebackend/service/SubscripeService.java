package com.mindspore.mindsporemanagebackend.service;

import com.mindspore.mindsporemanagebackend.pojo.Email;

import java.util.List;

public interface SubscripeService {

    List<Email> selectEmailByLang(String lang);

    List<Email> push();

    Integer countSubscription();

    Integer countCancelSubscription();

}
