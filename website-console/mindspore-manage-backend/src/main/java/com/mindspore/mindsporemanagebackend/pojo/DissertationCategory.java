package com.mindspore.mindsporemanagebackend.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator 论文分类
 */
@Data
public class DissertationCategory implements Serializable {
    private static final long serialVersionUID = 1902922119638498339L;

    public static final String POPULAR = "popular";
    public static final String DOMAIN = "domain";
    public static final String SOURCE = "source";
    private Integer id;

    /**
     * 名称
     */
    private String title;
    /**
     * 链接
     */
    private String link;
    /**
     * 图片
     */
    private String image;

    /**
     * 语言
     */
    private String lang;

    /**
     * 热门推荐- popular 领域-domain 来源-source
     */
    private String type;

    private Integer sequence;

    private String updateTime;
}
