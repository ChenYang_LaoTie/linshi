package com.mindspore.mindsporemanagebackend.common.util;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.aspectj.lang.JoinPoint;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import jakarta.servlet.http.HttpServletRequest;
import lombok.Data;

public class LogUtil {
    
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LogUtil.class);


    public static void managementOperate(JoinPoint joinPoint, int status, String message, HttpServletRequest request) {
        ManagementLog log = new ManagementLog();
        log.setType("Official website operation");

        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        log.setTime(dateTime.format(formatter));

        log.setFunc(String.format("%s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));

        log.setRequestUrl(request.getRequestURI());
        log.setMethod(request.getMethod());

        log.setAppIP(ClientIPUtil.getClientIpAddress(request));

        log.setStatus(status);
        log.setMessage(message);

        String jsonLog = JSON.toJSONString(log);
        logger.info("operationLog:{}", jsonLog);
    }

    @Data
    public static class ManagementLog implements Serializable {

        @Serial
        private static final long serialVersionUID = 1L;

        private String type;

        private String time;

        private String func;

        private String eventDetails;

        private String requestUrl;

        private String method;

        private String appIP;

        private int status;

        private String message;

        private String ErrorLog;

    }
}
