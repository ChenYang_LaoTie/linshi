package com.mindspore.mindsporemanagebackend.mapper;

import com.mindspore.mindsporemanagebackend.pojo.entity.Competition;
import com.mindspore.mindsporemanagebackend.pojo.entity.CompetitionPopular;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

public interface CompetitionMapper {

    void saveOrUpdateCompetition(Competition competition);

    void deleteCompetition(@Param("ids") Integer[] ids);

    void batchPublishCompetition(@Param("ids") Integer[] ids);

    void saveOrUpdateCompetitionPopular(CompetitionPopular competitionPopular);

    @Delete("DELETE FROM competition_popular WHERE id = #{id}")
    void deleteCompetitionPopular(Integer id);

}
