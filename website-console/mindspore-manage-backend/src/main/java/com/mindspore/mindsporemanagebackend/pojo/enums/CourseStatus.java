package com.mindspore.mindsporemanagebackend.pojo.enums;

/**
 * 课程状态
 *
 * @author yuanyan
 * @version 0.1
 * @create 2021/11/4
 */
public enum CourseStatus {
    NOT_LISTED("0", "未上架"),
    ALREADY_LISTED("1", "已上架");

    private String status;

    private String description;

    CourseStatus(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
