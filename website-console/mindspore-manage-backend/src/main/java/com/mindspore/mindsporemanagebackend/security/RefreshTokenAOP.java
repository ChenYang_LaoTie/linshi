package com.mindspore.mindsporemanagebackend.security;

import com.mindspore.mindsporemanagebackend.security.vo.AccessToken;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Optional;

@Aspect
@Component
public class RefreshTokenAOP {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private HttpServletResponse httpServletResponse;

    @Pointcut("execution(* com.mindspore.mindsporemanagebackend.controller..*.*(..))")
    public void pointCut() {
    }


    @Before(value = "pointCut()")
    public void BeforeController() throws Exception {
        Cookie[] cookies = httpServletRequest.getCookies();
        Cookie refreshToken = null;
        if (cookies != null) {
            Optional<Cookie> first = Arrays.stream(cookies).filter(c -> OIDCConfig.REFRESH_TOKEN.equals(c.getName())).findFirst();
            if (first.isPresent()) {
                refreshToken = first.get();
            }
        }
        if (null != refreshToken) {
            refreshToken(httpServletResponse,refreshToken);
        }
    }

    private void refreshToken(HttpServletResponse response, Cookie refreshToken) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", OIDCConfig.REFRESH_TOKEN);
        map.add(OIDCConfig.REFRESH_TOKEN, refreshToken.getValue());

        HttpEntity<MultiValueMap<String, String>> tokenRequest = new HttpEntity<>(map, headers);

        ResponseEntity<AccessToken> tokenResponse = restTemplate.postForEntity(OIDCConfig.TOKEN_URL, tokenRequest , AccessToken.class);

        AccessToken accessToken = tokenResponse.getBody();

        if (null == accessToken) {
            throw new Exception("can not refresh token");
        }

        Cookie accessTokenCookie = new Cookie(OIDCConfig.ACCESS_TOKEN, accessToken.getAccessToken());
        accessTokenCookie.setPath("/");

        Cookie refreshTokenCookie = new Cookie(OIDCConfig.REFRESH_TOKEN, accessToken.getRefreshToken());
        refreshTokenCookie.setPath("/");

        response.addCookie(accessTokenCookie);
        response.addCookie(refreshTokenCookie);
    }

}
