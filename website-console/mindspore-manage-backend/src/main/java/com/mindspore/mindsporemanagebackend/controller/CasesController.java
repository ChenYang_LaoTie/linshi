package com.mindspore.mindsporemanagebackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mindspore.mindsporemanagebackend.common.constant.StringConstants;
import com.mindspore.mindsporemanagebackend.pojo.Cases;
import com.mindspore.mindsporemanagebackend.pojo.CasesCategory;
import com.mindspore.mindsporemanagebackend.service.CasesService;
import com.mindspore.mindsporemanagebackend.vo.FileVo;
import com.mindspore.mindsporemanagebackend.vo.PageObject;
import com.mindspore.mindsporemanagebackend.vo.SysResult;

import io.micrometer.common.util.StringUtils;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/source")
@CrossOrigin("*")
public class CasesController {


    @GetMapping("/aa")
    public String test(HttpServletRequest request, HttpServletResponse response) throws Exception{

        return "hello";
    }


    @Autowired
    private CasesService service;

    @RequestMapping(path = "/casescategory/dropdown", method = RequestMethod.GET)
    public SysResult getCasesCategoryDropDown() {
        return SysResult.ok(service.getCasesCategoryDropDown());
    }

    @RequestMapping(path = "/casescategory/add", method = RequestMethod.POST)
    public SysResult insertCasesCategory(CasesCategory casesCategory) {
        service.addCasesCategory(casesCategory);
        return SysResult.ok();
    }

    @RequestMapping(path = "/casescategory/info", method = RequestMethod.GET)
    public SysResult findCasesCategory(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要查询的案例目录", null);
        }
        CasesCategory result = service.findCasesCategory(id);
        return SysResult.ok(result);
    }

    @RequestMapping(path = "/casescategory/delete", method = RequestMethod.GET)
    public SysResult deleteCasesCategory(Integer id) {
        if (id == null) {
            return SysResult.fail("请先选择要删除的案例目录", null);
        }
        String result = service.deleteCasesCategory(id);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("案例目录删除失败", result);
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/cases/image/uploadFile", method = RequestMethod.POST)
    public FileVo uploadCasesImageFile(MultipartFile uploadFile) {
        return service.upload(uploadFile, StringConstants.CASES_LOGO);
    }

    @RequestMapping(path = "/cases/add", method = RequestMethod.POST)
    public SysResult insertCases(Cases cases) {
        if (cases == null || StringUtils.isEmpty(cases.getTitle())) {
            return SysResult.fail("title is null", null);
        }
        cases.setStatus("0");
        if (cases.getSequenceNo() == null) {
            cases.setSequenceNo(0);
        }
        if (cases.getTitle().contains("&amp;")) {
            cases.setTitle(cases.getTitle().replace("amp;", ""));
        }
        service.addCases(cases);
        return SysResult.ok();
    }

    @RequestMapping(path = "/cases/update", method = RequestMethod.POST)
    public SysResult updateCases(Cases cases) {
        if (cases == null || StringUtils.isEmpty(cases.getTitle())) {
            return SysResult.fail("title is null", null);
        }
        if (cases.getId() == null) {
            return SysResult.fail("id is null", null);
        }
        if (cases.getTitle().contains("&amp;")) {
            cases.setTitle(cases.getTitle().replace("amp;", ""));
        }
        service.updateCases(cases);
        return SysResult.ok();
    }

    @RequestMapping(path = "/cases/list", method = RequestMethod.GET)
    public PageObject<Cases> getCasesList(Integer pageCurrent, Integer limit, String name, Integer categoryId) {
        return service.getCasesList(pageCurrent, limit, name, categoryId);
    }

    @RequestMapping(path = "/cases/release", method = RequestMethod.GET)
    public SysResult releaseCases(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要上架的案例", null);
        }
        String result = service.releaseCases(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("案例上架失败", "");
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/cases/unshelf", method = RequestMethod.GET)
    public SysResult unshelfCases(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要下架的案例", null);
        }
        String result = service.unshelfCases(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("案例下架失败", "");
        }
        return SysResult.ok();
    }

    @RequestMapping(path = "/cases/delete", method = RequestMethod.GET)
    public SysResult deleteCases(Integer... ids) {
        if (ids == null) {
            return SysResult.fail("请先选择要删除的案例", null);
        }
        String result = service.deleteCases(ids);
        if (!StringUtils.isEmpty(result)) {
            return SysResult.fail("案例删除失败", "");
        }
        return SysResult.ok();
    }
}
